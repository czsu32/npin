# NPIN model: Identification of Neuronal Polarity by Node-Based Machine Learning
We provide here a a powerful machine learning algorithm: node-based polarity identifier of neurons (NPIN). The proposed 
model is trained by nodal information only and includes both Soma Features (which contain spatial information from a 
given node to a soma) and Local Features (which contain morphological information of a given node). 
This package also include our training data, 213 neurons from FlyCircuit. 
For more information please check out our paper〈[Identification of Neuronal Polarity by Node-Based Machine Learning](https://arxiv.org/abs/2006.12148)〉.

#### Code Authors
Chen-Zhi Sua, Kuan-Ting Chou, and Chiau-Jou Li.


### Folders: 
* Original data: "./data/nrn_original" (.swc)
* Cleaned data: "./data/nrn_cleaned"
* Training results: "./data/nrn_result"
* Predict results: "./data/nrn_pred"
* ML models: "./data/nrn_model"
* Plots: "./data/nrn_plot"


### Modules:
Flow: Data_Cleaner --> Create_Axon_Feature --> Prepared_Axon --> Classify_Axon  

* Data_Cleaner::load_data(): turn swc skeletons into level trees.

* Create_Axon_Feature::load_data(): create Soma-features and Local-features.

* Data_Augmentation::create_axon_featrue(): create features for fake neurons.

* Prepared_Axon::load_data(): prepare input data.
  
* Classify_Axon::
  * classify_data(): train and test over the data.
  
  * predict(): predict new data.
 
   * evaluate(): evaluate the trained model.
  <!-- ![alt text](img/evaluate.png) -->
  <img src="img/evaluate.png"  width="400">
  
  * plot_result():
  <!-- ![alt text](img/plot_result.png) -->
  <img src="img/plot_result.png"  width="400">
  
  * correct_distribution(): x: the accuracy of a nueron, y: the number of neurons under certain accuracy. Title: the name of the set of neurons (total neuron number).
  <!-- ![alt text](img/correct_distribution.png) -->
  <img src="img/correct_distribution.png"  width="400">
  
  * analyze_feature(): display the axon/dendrite distribution by a certain feature. Title: the name of the set of neurons (ftr importance in the model) (total node number).
  <!-- ![alt text](img/analyze_feature.png) -->
  <img src="img/analyze_feature.png"  width="400">
    
* Plot_Tree::plot_tree(): plot level trees.
<!-- ![alt text](img/plot_tree.png) -->
<img src="img/plot_tree.png"  width="500">

