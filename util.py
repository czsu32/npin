import warnings

warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.filterwarnings("ignore")


# import lightgbm as lgb
import csv
import copy
import numpy as np
import pandas as pd  # only used to return a dataframe
import math
import neurom as nm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from graphviz import Digraph
import datetime
import os, errno, sys, shutil
from collections import OrderedDict
import matplotlib.ticker as ticker
import pylab
import time
import progressbar
import itertools as it
from sklearn.manifold import TSNE
from sklearn import preprocessing
from sklearn.neighbors.kde import KernelDensity
from sklearn.base import clone
from sklearn.linear_model import RidgeClassifier
from sklearn.svm import SVC
from sklearn import ensemble
from sklearn.metrics import mean_squared_error, accuracy_score, precision_score, recall_score, f1_score, r2_score, classification_report, confusion_matrix, classification_report
from sklearn.model_selection import ShuffleSplit, learning_curve
import functools
import sys
from xgboost import XGBClassifier
from settings import *
import heapq
# import h2o
from scipy.stats import norm
from scipy.optimize import brentq
from scipy.spatial import distance
import ast
import random
from tqdm import tqdm
import pickle
import tensorflow as tf
from sys import platform
import gc
from scipy.spatial import ConvexHull


# pd.set_option('display.width', 1000)
pd.set_option('display.max_colwidth', 160)
np.set_printoptions(linewidth=10000)

# pd.set_option('display.height', 1000)
# pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

########################################################################################################################
def find_distanceX_of_maxNumY(df_dis, tree_node_dict, dis_depend_on='all', decimal=0, view=False, save_path=None):
    leaf_lst = tree_node_dict['leaf']

    if dis_depend_on == 'all':
        ax = sns.distplot(np.asarray(df_dis['len']), hist_kws={"ec": "k"}, label='total')

    elif dis_depend_on == 'leaf':
        leaf_dis = df_dis.loc[df_dis['descendant'].isin(leaf_lst), 'len'].values
        ax = sns.distplot(leaf_dis, hist_kws={"ec": "k"}, label="leaf")

    else:
        sys.exit("\n Use 'all' or 'leaf' for dis_depend_on parameter in find_distanceX_of_maxNumY().")

    '''
    ex.
    x = np.random.randn(100)
    ax = sns.distplot(x, hist_kws={"ec": "k"})
    data_x, data_y = ax.lines[0].get_data()
    '''
    data_x, data_y = ax.lines[0].get_data()

    max_y = max(data_y)  # Find the maximum y value
    max_x = data_x[data_y.argmax()]  # Find the x value corresponding to the maximum y value
    pylab.text(max_x, max_y, str((max_x, max_y)))
    ax.plot([max_x], [max_y], marker="o")

    # xi = 0 # coordinate where to find the value of kde curve
    # yi = np.interp(xi,data_x, data_y)
    # print ("x={},y={}".format(xi, yi)) # prints x=0,y=0.3698
    # ax.plot([xi],[yi], marker="o")

    if view:
        plt.show()
    if save_path is not None:
        plt.savefig(save_path)

    plt.close()

    if decimal is None:
        return max_x
    elif decimal == 0:
        return int(round(max_x))
    else:
        return round(max_x, decimal)


########################################################################################################################
def remove_small_leaf(df, df_dis, tree_node_dict, origin_cols=['x',  'y',  'z',  'R',  'T',  'ID',  'PARENT_ID'],
                      less_than=None, less_than_equal_to=None):
    leaf_lst = tree_node_dict['leaf']

    if less_than is not None:
        small_leaf = df_dis.loc[(df_dis['len'] < less_than) & (df_dis['descendant'].isin(leaf_lst)),
                                'descendant'].tolist()
    elif less_than_equal_to is not None:
        small_leaf = df_dis.loc[(df_dis['len'] <= less_than_equal_to) & (df_dis['descendant'].isin(leaf_lst)),
                                'descendant'].tolist()
    elif all([less_than is not None, less_than_equal_to is not None]):
        small_leaf = df_dis.loc[(df_dis['len'] <= less_than_equal_to) & (df_dis['descendant'].isin(leaf_lst)),
                                'descendant'].tolist()
    else:
        sys.exit('\n No distance threshold in remove_small_leaf().')

    df = df[~df['branch'].isin(small_leaf)].reset_index(drop=True)
    df = df[origin_cols]

    return df


########################################################################################################################
def neuron_tree_node_dict(df, child_col='ID', parent_col='PARENT_ID', childNum_col='NC'):
    ### Create list of leaf/fork/root
    leaf_lst = df.loc[df[childNum_col] == 0, child_col].tolist()
    fork_lst = df.loc[df[childNum_col] > 1, child_col].tolist()
    root_lst = df.loc[df[parent_col] == -1, child_col].tolist()
    if len(root_lst) == 1:
        pass
    else:
        sys.exit("\n Multiple roots(somas) in a neuron. Check 'neuron_num_of_child()'.")

    tree_node_dict = {'root': root_lst, 'fork': fork_lst, 'leaf': leaf_lst}

    return tree_node_dict


########################################################################################################################
def neuron_childNumCol(df, child_col='ID', parent_col='PARENT_ID', output_col='NC'):
    ### Create child number col
    df_freq = pd.value_counts(df[parent_col]).to_frame().reset_index()
    df_freq.columns = [child_col, output_col]
    df_freq = df_freq.sort_values([child_col]).reset_index(drop=True)

    df = pd.merge(df, df_freq, how='left', on=child_col)
    df[output_col] = np.where(np.isnan(df[output_col]), 0, df[output_col])
    df[output_col] = df[output_col].astype(int)

    ### Create list of leaf/fork/root
    tree_node_dict = neuron_tree_node_dict(df, child_col, parent_col, childNum_col=output_col)

    return df, tree_node_dict


########################################################################################################################
def neuron_ancestors_and_path(df, child_col='ID', parent_col='PARENT_ID'):
    df_anc = df[[child_col, parent_col]]

    ### Need to drop row that "PARENT_ID = -1" first
    df_anc = df_anc[df_anc[parent_col] != -1]

    edges = df_anc.values

    ancestors = []
    path = []
    for ar in trace_nodes(edges):
        ancestors.append(np.c_[np.repeat(ar[:, 0], ar.shape[1] - 1),
                               ar[:, 1:].flatten()])
        path.append(np.c_[np.repeat(ar[:, 0], ar.shape[1]),
                          ar[:, :].flatten()])

    return pd.DataFrame(np.concatenate(ancestors),columns=['descendant', 'ancestor']), \
           pd.DataFrame(np.concatenate(path),columns=['descendant', 'path'])


########################################################################################################################
def numpy_col_inner_many_to_one_join(ar1, ar2):
    """
    Take two 2-d numpy arrays ar1 and ar2,
    with no duplicate values in first column of ar2
    Return inner join of ar1 and ar2 on
    last column of ar1, first column of ar2

    Ex:

        ar1 = np.array([[1,  2,  3],
                        [4,  5,  3],
                        [6,  7,  8],
                        [9, 10, 11]])

        ar2 = np.array([[ 1,  2],
                        [ 3,  4],
                        [ 5,  6],
                        [ 7,  8],
                        [ 9, 10],
                        [11, 12]])

        numpy_col_inner_many_to_one_join(ar1, ar2)

        returns

        array([[ 1,  2,  3,  4],
               [ 4,  5,  3,  4],
               [ 9, 10, 11, 12]])
    """
    # Select connectable rows of ar1 and ar2 (ie. ar1 last_col = ar2 first col)
    ar1 = ar1[np.in1d(ar1[:, -1], ar2[:, 0])]   # error occurred if ar1 is empty.
    ar2 = ar2[np.in1d(ar2[:, 0], ar1[:, -1])]

    # if int >= 0, else otherwise
    if 'int' in ar1.dtype.name and ar1[:, -1].min() >= 0:
        bins = np.bincount(ar1[:, -1])
        counts = bins[bins.nonzero()[0]]
    else:
        # order of np is "-int -> 0 -> +int -> other type"
        counts = np.unique(ar1[:, -1], False, False, True)[1]

    # Reorder array with np's order rule
    left = ar1[ar1[:, -1].argsort()]
    right = ar2[ar2[:, 0].argsort()]

    # Connect the rows of ar1 & ar2
    return np.concatenate([left[:, :-1],
                           right[np.repeat(np.arange(right.shape[0]),
                                           counts)]], 1)

########################################################################################################################
def trace_nodes(edges):
    """
    Take edge list of a rooted tree as a numpy array with shape (E, 2),
    child nodes in edges[:, 0], parent nodes in edges[:, 1]
    Yield numpy array with cross-section of tree and associated
    ancestor nodes

    Ex:
        df = pd.DataFrame({'child': [200, 201, 300, 301, 302, 400],
                           'parent': [100, 100, 200, 200, 201, 300]})

        df
           child  parent
        0    200     100
        1    201     100
        2    300     200
        3    301     200
        4    302     201
        5    400     300

        trace_nodes(df.values)

        yields

        array([[200, 100],
               [201, 100]])

        array([[300, 200, 100],
               [301, 200, 100],
               [302, 201, 100]])

        array([[400, 300, 200, 100]])
    """
    # Top layer
    mask = np.in1d(edges[:, 1], edges[:, 0])    # parent with/without further ancestor
    gen_branches = edges[~mask]   # branch of parent without further ancestor
    edges = edges[mask]   # branch of otherwise
    yield gen_branches    # generate result

    # Successor layers
    while edges.size != 0:
        mask = np.in1d(edges[:, 1], edges[:, 0])    # parent with/without further ancestor
        next_gen = edges[~mask]   # branch of parent without further ancestor
        gen_branches = numpy_col_inner_many_to_one_join(next_gen, gen_branches)  # connect with further ancestors
        edges = edges[mask]   # branch of otherwise
        yield gen_branches    # generate result


########################################################################################################################
def neuron_level_branch(df_path, tree_node_dict):
    leaf_lst = tree_node_dict['leaf']
    fork_lst = tree_node_dict['fork']
    root_lst = tree_node_dict['root']
    ### Create branches (level tuple list)
    level_points = list(set().union(leaf_lst, fork_lst, root_lst))
    df_level = df_path.loc[df_path['path'].isin(level_points)]
    df_level = df_level.loc[df_level['descendant'].isin(leaf_lst)]
    df_level = df_level.reset_index(drop=True)
    # df_level = df_level.sort_values(['descendant', 'path']).reset_index(drop=True)

    branches = []
    for i in df_level.descendant.unique().tolist():
        lst = df_level.loc[df_level['descendant'] == i, 'path'].tolist()
        branches = list(set().union(branches, zip(lst, lst[1:])))

    return branches


########################################################################################################################
def neuron_first_fork(root_lst, fork_lst, branch_lst):
    soma = root_lst[0]
    if soma in fork_lst:
        first_fork = soma
    else:
        result = [t for t in branch_lst if all([t[1] == soma, t[0] in fork_lst])]
        if len(result) == 1:
            first_fork = result[0][0]
        elif len(result) == 0:
            sys.exit("\n No fork in the neuron! Check 'neuron_first_fork()'.")
        else:
            sys.exit("\n Multiple first_fork in the neuron! Check 'neuron_first_fork()'.")

    return first_fork


########################################################################################################################
def zero_list_maker(n):
    list_of_zeros = [0] * n
    return list_of_zeros

########################################################################################################################
def neuron_levelCol(df, df_anc, df_path, tree_node_dict, branch_lst, child_col='ID', parent_col='PARENT_ID'):
    ### cihld_col is from "df"
    '''
    df = pd.DataFrame({'ID': [1, 2, 3, 100, 200, 201, 300, 301, 302, 400],
                       'PARENT_ID': [-1, 1, 2, 3, 100, 100, 200, 200, 201, 300]})
    df = pd.DataFrame({'ID': [100, 200, 201, 300, 301, 302, 400],
                       'PARENT_ID': [-1, 100, 100, 200, 200, 201, 300]})
    '''
    leaf_lst = tree_node_dict['leaf']
    fork_lst = tree_node_dict['fork']
    root_lst = tree_node_dict['root']

    ### 1. Points btw root and first_fork
    first_fork = neuron_first_fork(root_lst, fork_lst, branch_lst)
    temp_lst = df_anc.loc[df_anc['descendant'] == first_fork, 'ancestor'].tolist()
    temp_lst.append(first_fork)
    zero_lst = zero_list_maker(len(temp_lst))
    df_temp_1 = pd.DataFrame({child_col: temp_lst, 'level': zero_lst})

    ### 2. Points after first_fork
    df_temp_2 = df_anc.loc[df_anc['ancestor'].isin(fork_lst)]
    df_temp_2 = pd.value_counts(df_temp_2.descendant).to_frame().reset_index()
    df_temp_2.columns = [child_col, 'level']

    ### Merge 1. & 2. to df
    df_temp_1 = pd.concat([df_temp_1, df_temp_2])
    df_temp_1 = df_temp_1.sort_values([child_col]).reset_index(drop=True)

    df = pd.merge(df, df_temp_1, how='left', on=child_col)

    max_level = max(df['level'])


    ### 3. Find deepest level of each point
    df['dp_level'] = 0
    # df_path = df_path.loc[df_path['path'] != 1]

    df_temp = df.loc[df[child_col].isin(leaf_lst), [child_col, 'level']].sort_values(['level'], ascending=False).reset_index(drop=True)
    for l in df_temp[child_col]:
        path = df_path.loc[df_path['descendant'] == l, 'path'].tolist()
        level = df_temp.loc[df_temp[child_col] == l, 'level'].values[0]
        df['dp_level'] = np.where((df[child_col].isin(path)) & (df['dp_level'] < level),
                                  level, df['dp_level'])



    return df, max_level, first_fork


########################################################################################################################
def calculate_distance(positions, decimal=None, type='euclidean'):
    '''
    ex.
    positions=[(0, 0), (3, 4), (7, 7)]
    positions=[(0, 0, 0), (3, 4, 0), (3, 16, 9)]
    '''

    results = []

    # Detect dimension of tuples in the positions
    try:
        if all(len(tup) == 2 for tup in positions):
            dim = 2
        elif all(len(tup) == 3 for tup in positions):
            dim = 3
    except:
        print('Dimension of positions must be same in calculate_distance()!')


    # Calculate distance
    try:
        if all([dim == 2, type == 'haversine']):
            for i in range(1, len(positions)):
                loc1 = positions[i - 1]
                loc2 = positions[i]

                lat1 = loc1[0]
                lng1 = loc1[1]

                lat2 = loc2[0]
                lng2 = loc2[1]

                degreesToRadians = (math.pi / 180)
                latrad1 = lat1 * degreesToRadians
                latrad2 = lat2 * degreesToRadians
                dlat = (lat2 - lat1) * degreesToRadians
                dlng = (lng2 - lng1) * degreesToRadians

                a = math.sin(dlat / 2) * math.sin(dlat / 2) + math.cos(latrad1) * \
                math.cos(latrad2) * math.sin(dlng / 2) * math.sin(dlng / 2)
                c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
                r = 6371000

                results.append(r * c)

        elif all([dim == 2, type == 'euclidean']):
            for i in range(1, len(positions)):
                loc1 = positions[i - 1]
                loc2 = positions[i]

                x1 = loc1[0]
                y1 = loc1[1]

                x2 = loc2[0]
                y2 = loc2[1]

                d = np.sqrt((x2-x1)**2 + (y2-y1)**2)

                results.append(d)

        elif all([dim == 3, type == 'euclidean']):
            for i in range(1, len(positions)):
                loc1 = positions[i - 1]
                loc2 = positions[i]

                x1 = loc1[0]
                y1 = loc1[1]
                z1 = loc1[2]

                x2 = loc2[0]
                y2 = loc2[1]
                z2 = loc2[2]

                d = np.sqrt((x2-x1)**2 + (y2-y1)**2 + (z2-z1)**2)

                results.append(d)


        if decimal is None:
            return sum(results)
        elif decimal == 0:
            return int(round(sum(results)))
        else:
            return round(sum(results), decimal)

    except:
        print('Please use available type and dim, such as "euclidean"(2-dim, 3-dim) and "haversine" (2-dim only), '
              'in calculate_distance().')


########################################################################################################################
def neuron_branchCol_QCol_distance(df, df_path, tree_node_dict, branch_lst, first_fork, decimal=0,
                                   child_col='ID', parent_col='PARENT_ID', type_col='T'):
    ### Create distances of branches and create Q col
    length_lst = []    # distance of branch
    length_lst_soma = []  # distance of descendant to soma
    direct_dis_lst_soma = []  # direct distance of descendant to soma
    df_temp = pd.DataFrame()
    for i in branch_lst:
        start = i[0]
        end = i[1]
        soma = tree_node_dict['root'][0]

        path_points = df_path.loc[df_path['descendant'] == start, 'path'].tolist()

        start_idx = path_points.index(start)
        end_idx = path_points.index(end)
        soma_idx = path_points.index(soma)

        path_points_1 = path_points[start_idx: end_idx]         # exclude the end point(for Q)
        path_points_2 = path_points[start_idx: (end_idx + 1)]   # include the end point(for Q, dis)
        path_points_3 = path_points[start_idx: (soma_idx + 1)]  # path to soma


        # Create branch col and Q col
        # branch with end pt != 1 or first_fork == 1 (first_fork == soma)
        if any([end != 1, first_fork == 1]):
            temp_lst_1 = [start] * len(path_points_1)
            temp_lst_2 = list(range(len(path_points_1)))
            df_temp_1 = pd.DataFrame(OrderedDict({child_col: path_points_1, 'branch': temp_lst_1, 'Q': temp_lst_2}))
            df_temp = df_temp.append(df_temp_1)
        # end pt == 1 & first_fork != 1 (first_fork != soma)
        else:
            temp_lst_1 = [start] * len(path_points_2)
            temp_lst_2 = list(range(len(path_points_2)))
            df_temp_1 = pd.DataFrame(OrderedDict({child_col: path_points_2, 'branch': temp_lst_1, 'Q': temp_lst_2}))
            df_temp = df_temp.append(df_temp_1)


        # Calculate distance
        positions = df.loc[df[child_col].isin(path_points_2), ['x', 'y', 'z']]
        tuples = [tuple(x) for x in positions.values]
        positions_s = df.loc[df[child_col].isin(path_points_3), ['x', 'y', 'z']]
        tuples_s = [tuple(x) for x in positions_s.values]
        tuples_ds = [tuples_s[0], tuples_s[-1]]

        length = calculate_distance(tuples, decimal)
        length_lst.append(length)

        length_soma = calculate_distance(tuples_s, decimal)
        length_lst_soma.append(length_soma)

        direct_dis_soma = calculate_distance(tuples_ds, decimal)
        direct_dis_lst_soma.append(direct_dis_soma)


    # Merge branch & Q into original df
    df = pd.merge(df, df_temp, how='left', on=child_col)
    if first_fork == 1:
        df.loc[df[parent_col] == -1, ['branch', 'Q']] = [1, 0]  # add soma if first_fork = 1
    df[['branch', 'Q']] = df[['branch', 'Q']].astype('int')


    ### Create df_dis (cols ['len_des_soma', 'des_T'])
    df_dis = pd.DataFrame({'branch': branch_lst, 'len': length_lst, 'len_des_soma': length_lst_soma, 'direct_dis_des_soma': direct_dis_lst_soma})
    df_dis['ancestor'] = [tuple[1] for tuple in branch_lst]
    df_dis['descendant'] = [tuple[0] for tuple in branch_lst]
    df_dis = df_dis.sort_values(['ancestor', 'descendant']).reset_index(drop=True)
    # # create dis_anc_soma
    # df_anc = df_dis[['descendant', 'len_des_soma']]
    # df_anc = df_anc.rename({'descendant':'ancestor', 'len_des_soma':'dis_anc_soma'})
    # df_dis = pd.merge(df_dis, df_anc, how='left', on='descendant')

    # create type column
    df_t = df[[child_col, type_col]].copy()
    df_t.columns = ['descendant', 'des_T']
    df_dis = pd.merge(df_dis, df_t, how='left', on='descendant')


    ### Reorder columns
    df_dis = df_dis[['branch', 'descendant', 'ancestor', 'len', 'len_des_soma', 'direct_dis_des_soma', 'des_T']]


    # print(df_dis)
    # neuron_plot_relation_tree(df_dis, 'des_point', 'anc_point', 'len', save=True, filename='level')

    return df, df_dis, length_lst


########################################################################################################################
def update_parent_col(df, df_dis, tree_node_dict, child_col, parent_col, select_node=["root", "fork", "leaf"]):
    # Select nodes
    if set(select_node).issubset(set(["root", "fork", "leaf"])):
        tn_lst = []
        for i in select_node:
            tn_lst += tree_node_dict[i]
        df = df.loc[df[child_col].isin(tn_lst)]
    else:
        sys.exit("\n select_node should be in this range ['root', 'fork', 'leaf']! Check update_parent_col().")
    # Merge df_dic ancestor to df
    _info = df_dis.loc[:, ['descendant', "ancestor"]]
    _info = _info.rename(columns={'descendant': child_col})
    df = pd.merge(df, _info, how="left", on=child_col)
    # Update root's parent
    df[parent_col] = np.where(df[child_col] == tree_node_dict["root"][0], -1, df["ancestor"])
    # Clean up
    df = df.drop(["ancestor"], 1)
    df[[parent_col]] = df[[parent_col]].astype('int')

    return df


########################################################################################################################
def pre_relabel(df):
    """
        Define new label to increase the quantity of data

        :param df:
            type: DataFrame
            only permit df of one neuron list (level tree)

        :return:
            type: DataFrame
            the input data with a new column: type_pre
    """

    def index_search(lst, value):
        if value == -1:
            return -1
        index = -1

        for i in range(len(lst)):
            if lst[i][0] == value:
                index = i
                break

        return index

    # load information
    df_p = df.loc[:, ['ID', 'PARENT_ID', 'T', 'NC']]
    df_lst = df_p.values.tolist()

    # modify the original type
    for i in range(len(df_lst)):
        if df_lst[i][2] == 0:
            continue
        elif df_lst[i][2] == 1:
            continue
        elif df_lst[i][2] == 2 or df_lst[i][2] // 10 == 2 or df_lst[i][2] == -2:
            df_lst[i][2] = 2
            continue
        elif df_lst[i][2] == 3 or df_lst[i][2] // 10 == 3 or df_lst[i][2] == -3:
            df_lst[i][2] = 3
            continue
        elif df_lst[i][2] == 4 or df_lst[i][2] // 10 == 4 or df_lst[i][2] == -4:
            df_lst[i][2] = 4
            continue
        else:
            print("label problem")
            print(df_lst[i][2])

    # pre-relabel
    buffer_c = []  # terminal node list index
    buffer_p = []  # parent nodes list index
    res = [0 for i in range(len(df_lst))]
    # 1. find terminal nodes, i.e. NC=0
    for i in range(len(df_lst)):
        if df_lst[i][3] == 0:
            res[i] = df_lst[i][2]  # record its type
            buffer_c.append(i)  # record its idx
    # 2.
    while len(buffer_c) != 0:
        buffer_3 = []
        # find the parent node for each terminal
        for i in buffer_c:
            buffer_p.append(index_search(df_lst, df_lst[i][1]))
        buffer = set(buffer_p)
        #
        for i in buffer:
            # if the parent node has only 1 child
            if buffer_p.count(i) == df_lst[i][3]:
                # csu todo
                location = [j for j, v in enumerate(buffer_p) if v == i]
                buffer_3 = buffer_3 + location
                buffer_c.append(i)

                buffer_2 = []
                for j in range(len(location)):
                    buffer_2.append(res[buffer_c[location[j]]])

                unlabel_count = buffer_2.count(0)
                axon_count = buffer_2.count(2)
                dendrite_count = buffer_2.count(3)
                mix_count = buffer_2.count(4)
                if unlabel_count >= 1:
                    if axon_count >= 1 and dendrite_count == 0:
                        result = 2
                    elif axon_count == 0 and dendrite_count >= 1:
                        result = 3
                    else:
                        result = 0
                elif mix_count >= 2:
                    result = 4
                elif mix_count == 1:
                    if axon_count >= 1 and dendrite_count >= 1:
                        result = 4
                    elif axon_count >= 1 and dendrite_count == 0:
                        result = 2
                    elif axon_count == 0 and dendrite_count >= 1:
                        result = 3
                    else:
                        # ktc todo what is this? (run Data_Cleaner)
                        # --> The input data should be level tree rather than original tree
                        print("problem : pre-relabeling")
                        print("problem node: %d, types: %s" % (df_lst[i][0], str(buffer_2)))
                else:
                    if axon_count == 0:
                        result = 3
                    elif dendrite_count == 0:
                        result = 2
                    else:
                        result = 4

                res[i] = result
        buffer_3.sort()
        buffer_3.reverse()
        for j in buffer_3:
            buffer_c.pop(j)
        if 0 in buffer_c:
            location = [j for j, v in enumerate(buffer_c) if v == 0]
            location.reverse()
            for j in location:
                buffer_c.pop(j)
        buffer_p = []

    # soma
    if df_lst[0][2] == 1:
        res[0] = 1

    # adding pre-label column
    df['type_pre'] = res

    return df


########################################################################################################################
def neuron_polarity_dict(df, child_col='ID', type_col='T',
                         axon=[2, 20, 21, 22, 23], dendrite=[3, 30, 31, 32, 33], mix_point=[4]):

    axon_lst = df.loc[df[type_col].isin(axon), child_col].tolist()
    den_lst = df.loc[df[type_col].isin(dendrite), child_col].tolist()
    # a_d_lst = df.loc[df[type_col].isin(axon_drte), child_col].tolist()
    mix_lst = df.loc[df[type_col].isin(mix_point), child_col].tolist()

    polarity_dict = {'axon': axon_lst, 'dendrite': den_lst, 'axon_drte': [], 'mix': mix_lst}

    return polarity_dict


########################################################################################################################
def list_freq(lst):
    d = {}
    for i in lst:
        if d.get(i):
            d[i] += 1
        else:
            d[i] = 1
    return d


########################################################################################################################
def remove_extra_branch(df, df_dis, tree_node_dict, child_col, parent_col, target_branch=2):
    if target_branch < 2:
        sys.exit("\n 'target_branch' must >= 2! Check remove_extra_branch().")

    df_anc, df_path = neuron_ancestors_and_path(df, child_col, parent_col)

    # Detect branch
    a0 = df_dis["ancestor"].tolist()
    a0 = list_freq(a0)
    a0 = [k for k, v in a0.items() if float(v) > target_branch]

    # Remove branch
    if not a0:
        pass
    else:
        a0 = sorted(a0, reverse=True)
        for p0 in a0:
            # Find len
            child_lst = df_dis.loc[df_dis["ancestor"]==p0, "descendant"].tolist()
            df_temp = df.loc[df[child_col].isin(child_lst), [child_col, "dp_level"]]
            df_temp['len'] = 0
            for c0 in child_lst:
                d0 = df_path.loc[(df_path['descendant'].isin(tree_node_dict["leaf"])) & (df_path['path'].isin([c0]))]
                d0 = pd.merge(d0, df_dis[['descendant', 'len_des_soma']], how='left', on='descendant')
                mxlen = d0['len_des_soma'].max()
                df_temp['len'] = np.where(df_temp[child_col]==c0, mxlen, df_temp['len'])
            df_temp = df_temp.sort_values('len', ascending=False)

            # Remove branch
            remove_lst = df_temp[child_col].tolist()
            remove_lst = remove_lst[2:]
            for point in remove_lst:
                path_points = df_anc.loc[df_anc['ancestor']==point, 'descendant'].tolist() + [point]
                df = df.loc[~df[child_col].isin(path_points)]
                df_dis = df_dis.loc[~df_dis['descendant'].isin(path_points)]

            # Update NC
            df["NC"] = np.where(df[child_col] == p0, 2, df["NC"])

            # Update dp_level
            dp_lst = df_temp["dp_level"].tolist()
            max_dp = max(dp_lst[:2])
            df["dp_level"] = np.where(df[child_col] == p0, max_dp, df["dp_level"])

        df = df.reset_index(drop=True)
        df_dis = df_dis.reset_index(drop=True)


    return df, df_dis


########################################################################################################################
def shape_features(df, reduced_df):
    """
        Give features of shape

        :param df:
            type: DataFrame
            only permit df of one neuron list (level tree)
        :param reduced_df:
            type: DataFrame
            only permit df of one neuron list (reduced tree)
            direct use the format of pickle

        :return:
            type: DataFrame
            the input data with new features: ratio_ortho, curvature
    """

    def index_search(lst, value):
        if value == -1:
            return -1
        index = -1

        for i in range(len(lst)):
            if lst[i][0] == value:
                index = i
                break

        return index

    def children_search(lst, value):
        index = []
        for i in range(len(lst)):
            if lst[i][1] == value:
                index.append(i)
        return index

    # load information
    reduced_list = reduced_df.tree_node_dict["fork"] + reduced_df.tree_node_dict["leaf"]
    df_p = df.loc[:, ['ID', 'PARENT_ID', 'type_pre', 'NC', 'x', 'y', 'z', 'len', 'dp_level']]
    df_lst = df_p.values.tolist()
    for i in range(len(df_lst)):
        df_lst[i][0] = int(df_lst[i][0])
    df_lst.sort(key=(lambda x: (x[0])))

    # csu todo please check the result of the following filter
    # filter of reduced tree
    if df_lst[0][0] not in reduced_list:
        reduced_list.append(df_lst[0][0])

    temp = []
    for i in range(len(reduced_list)):
        index = index_search(df_lst, reduced_list[i])
        if df_lst[index][3] != 0:
            temp.append(reduced_list[i])
        index_c = children_search(df_lst, reduced_list[i])
        for j in index_c:
            if df_lst[j][3] != 0:
                temp.append(df_lst[j][0])
    reduced_list = copy.copy(temp)
    reduced_list.sort()
    del temp

    # features
    res = [[0 for i in range(len(df_lst))] for j in range(12)]
    for i in range(len(reduced_list)):
        index = int(index_search(df_lst, reduced_list[i]))
        buffer = []
        buffer.append(index)
        index = [index]
        change = True
        while change:
            change = False
            c_index = []
            for j in index:
                temp = children_search(df_lst, df_lst[j][0])
                c_index = c_index + temp
            index = copy.copy(c_index)
            if len(c_index) != 0:
                buffer = buffer + c_index
                change = True
        if len(buffer) == 1:
            continue

        # baseline: target node
        center_x = df_lst[buffer[0]][4]
        center_y = df_lst[buffer[0]][5]
        center_z = df_lst[buffer[0]][6]

        # create the moment of inertia
        I_xx = 0
        I_xy = 0
        I_xz = 0
        I_yy = 0
        I_yz = 0
        I_zz = 0

        for j in buffer:
            if j == buffer[0]:
                continue
            I_xx += (df_lst[j][5] - center_y) ** 2 + (df_lst[j][6] - center_z) ** 2
            I_xy -= (df_lst[j][4] - center_x) * (df_lst[j][5] - center_y)
            I_xz -= (df_lst[j][4] - center_x) * (df_lst[j][6] - center_z)
            I_yy += (df_lst[j][4] - center_x) ** 2 + (df_lst[j][6] - center_z) ** 2
            I_yz -= (df_lst[j][5] - center_y) * (df_lst[j][6] - center_z)
            I_zz += (df_lst[j][4] - center_x) ** 2 + (df_lst[j][5] - center_y) ** 2
        I_value, I_vector = np.linalg.eig(np.array([[I_xx, I_xy, I_xz],
                                                    [I_xy, I_yy, I_yz],
                                                    [I_xz, I_yz, I_zz]]))
        index = I_value.argmin()
        p_max = 0
        v_max = 0
        for j in buffer:
            if j == buffer[0]:
                continue
            v = np.array([df_lst[j][4] - center_x, df_lst[j][5] - center_y, df_lst[j][6] - center_z])
            l = np.dot(v, v)
            v_p = abs(np.dot(v, I_vector[:][index]))
            if v_p > p_max:
                p_max = v_p
            l = l - v_p**2
            l = math.sqrt(l)
            if l > v_max:
                v_max = l
        for j in buffer:
            #res[j][0] = p_max
            #res[j][1] = v_max
            res[0][j] = v_max / (p_max + v_max)

        p_max = [0, 0, 0]
        p_min = [0, 0, 0]
        for j in buffer:
            if j == buffer[0]:
                continue
            v = np.array([df_lst[j][4] - center_x, df_lst[j][5] - center_y, df_lst[j][6] - center_z])
            for k in range(I_vector.shape[1]):
                v_p = np.dot(v, I_vector[:][k])
                if v_p > p_max[k]:
                    p_max[k] = v_p
                if v_p < p_min[k]:
                    p_min[k] = v_p
            for k in range(len(p_max)):
                p_max[k] = p_max[k] - p_min[k]
            p_max.sort()
        for j in buffer:
            #res[j][0] = p_max
            #res[j][1] = v_max
            res[1][j] = p_max[-1] / (p_max[0] + p_max[-1])

        # baseline: center of mass
        center_x = 0
        center_y = 0
        center_z = 0
        for j in buffer:
            center_x += df_lst[j][4]/len(buffer)
            center_y += df_lst[j][5]/len(buffer)
            center_z += df_lst[j][6]/len(buffer)

        # create the moment of inertia
        I_xx = 0
        I_xy = 0
        I_xz = 0
        I_yy = 0
        I_yz = 0
        I_zz = 0

        for j in buffer:
            if j == buffer[0]:
                continue
            I_xx += (df_lst[j][5] - center_y) ** 2 + (df_lst[j][6] - center_z) ** 2
            I_xy -= (df_lst[j][4] - center_x) * (df_lst[j][5] - center_y)
            I_xz -= (df_lst[j][4] - center_x) * (df_lst[j][6] - center_z)
            I_yy += (df_lst[j][4] - center_x) ** 2 + (df_lst[j][6] - center_z) ** 2
            I_yz -= (df_lst[j][5] - center_y) * (df_lst[j][6] - center_z)
            I_zz += (df_lst[j][4] - center_x) ** 2 + (df_lst[j][5] - center_y) ** 2
        I_value, I_vector = np.linalg.eig(np.array([[I_xx, I_xy, I_xz],
                                                    [I_xy, I_yy, I_yz],
                                                    [I_xz, I_yz, I_zz]]))

        characteristic_length = []
        for j in range(len(I_value)):
            characteristic_length.append(math.sqrt(I_value[j] / len(buffer)))
        characteristic_length.sort()
        temp = characteristic_length[0]/characteristic_length[-1]
        for j in range(len(buffer)):
            res[11][j] = temp

        index = I_value.argmin()
        p_max = 0
        v_max = 0
        for j in buffer:
            if j == buffer[0]:
                continue
            v = np.array([df_lst[j][4] - center_x, df_lst[j][5] - center_y, df_lst[j][6] - center_z])
            l = np.dot(v, v)
            v_p = np.dot(v, I_vector[index])
            if v_p > p_max:
                p_max = v_p
            l = l - v_p**2
            l = math.sqrt(l)
            if l > v_max:
                v_max = l
        for j in buffer:
            # res[j][0] = p_max
            # res[j][1] = v_max
            res[2][j] = v_max / (p_max + v_max)

        p_max = [0, 0, 0]
        p_min = [0, 0, 0]
        for j in buffer:
            if j == buffer[0]:
                continue
            v = np.array([df_lst[j][4] - center_x, df_lst[j][5] - center_y, df_lst[j][6] - center_z])
            for k in range(I_vector.shape[1]):
                v_p = np.dot(v, I_vector[:][k])
                if v_p > p_max[k]:
                    p_max[k] = v_p
                if v_p < p_min[k]:
                    p_min[k] = v_p
            for k in range(len(p_max)):
                p_max[k] = p_max[k] - p_min[k]
            p_max.sort()
        for j in buffer:
            # res[j][0] = p_max
            # res[j][1] = v_max
            res[3][j] = p_max[-1] / (p_max[0] + p_max[-1])


        # Number in the range (under)
        temp_num = 0
        temp_curve = 0

        for j in buffer:
            if j == buffer[0]:
                continue
            length = (df_lst[buffer[0]][4]-df_lst[j][4])**2+(df_lst[buffer[0]][5]-df_lst[j][5])**2+(df_lst[buffer[0]][6]-df_lst[j][6])**2
            length = math.sqrt(length)

            temp_num += 1
            length_2 = df_lst[j][7]
            index_p = index_search(df_lst, df_lst[j][1])
            while index_p != buffer[0]:
                length_2 += df_lst[index_p][7]
                index_p = index_search(df_lst, df_lst[index_p][1])
            try:
                temp_curve += length_2/length
            except:
                pass
        '''
        for j in buffer:
            res[j][3] = temp_num
            res[j][4] = temp_curve
        '''
        for j in buffer:
            res[4][j] = temp_curve
        if temp_num != 0:
            temp_curve = temp_curve / temp_num
        else:
            temp_curve = 0
        for j in buffer:
            res[5][j] = temp_curve

        # Number in the range (under)
        dis_max = 0

        #for j in buffer:
        #    if j == buffer[0]:
        #        continue
        #    dis = (df_lst[buffer[0]][4] - df_lst[j][4]) ** 2 + (df_lst[buffer[0]][5] - df_lst[j][5]) ** 2 + (
        #                df_lst[buffer[0]][6] - df_lst[j][6]) ** 2
        #    dis = math.sqrt(dis)
        #    if dis > dis_max:
        #        dis_max = dis
        dis_max = math.pow(characteristic_length[0]*characteristic_length[1]*characteristic_length[2], 1.0/3)

        length = 0
        for j in buffer:
            if j == buffer[0]:
                continue
            length += df_lst[j][7]

        length2 = length + df_lst[buffer[0]][7]

        temp_curve = 0
        try:
            temp_curve += length / dis_max
        except:
            temp_curve = 0
        for j in buffer:
            res[6][j] = temp_curve
        del temp_curve

        temp_curve = 0
        try:
            temp_curve += length2 / dis_max
        except:
            temp_curve = 0
        for j in buffer:
            res[7][j] = temp_curve

        terminal_points = 0
        index = buffer[0]
        c_index = children_search(df_lst, df_lst[index][0])
        while len(c_index) != 0:
            index = []
            for i in c_index:
                temp = children_search(df_lst, df_lst[i][0])
                if len(temp) == 0:
                    terminal_points += 1
                else:
                    index = index + temp
            c_index = index

        # convex hull
        temp_lst = []
        if len(buffer) > 3:
            for j in buffer:
                temp_lst.append(df_lst[j][4:7])
            try:
                cv = ConvexHull(temp_lst)
                vol = cv.volume
                ave_vol_length = vol / length
                ave_vol_terminals = vol / terminal_points

                for j in buffer:
                    res[8][j] = vol
                    res[9][j] = ave_vol_length
                    res[10][j] = ave_vol_terminals
            except:
                print("Hull Problem")
                print(buffer)
                pass

    df['ratio_ortho'] = res[0]
    df['ratio2_ortho'] = res[1]
    df['ratio_com'] = res[2]
    df['ratio2_com'] = res[3]
    df['curvature_superposition'] = res[4]
    df['curvature_ave'] = res[5]
    df['curvature_r'] = res[6]
    df['curvature'] = res[7]
    df['volume'] = res[8]
    df['ave_volume_length'] = res[9]
    df['ave_volume_terminals'] = res[10]
    df['aspect_ratio'] = res[11]

    # ratio children
    res = [0 for i in range(len(df_lst))]
    for i in range(len(df_lst)):
        index_c = children_search(df_lst, df_lst[i][0])
        if len(index_c) == 0:
            res[i] = 0
            continue
        temp = 0
        temp_s = 0
        for j in index_c:
            temp_s += df_lst[j][7]
            if df_lst[j][7] > temp:
                temp = df_lst[j][7]
        if temp_s != 0:
            res[i] = temp/temp_s
        else:
            res[i] = 0
    df['ratio_children'] = res

    return df


########################################################################################################################
def get_fileNames_from_directory(directory='/Users/csu/Desktop/Neuron/nrn_original', file_type=None, drop_file_type=False):
    file_lst = []
    if directory.endswith("/"):
        directory = directory[:-1]

    for (dirpath, dirnames, filenames) in os.walk(directory):
        file_lst.extend(filenames)
        break

    if all([file_type is not None, file_type != 'all']):
        if type(file_type) is list:
            lst = []
            for t in file_type:
                lst_temp = [x for x in file_lst if x.endswith('.' + t)]
                if drop_file_type:
                    lst_temp = [x.split('.' + file_type)[0] for x in lst_temp]
                lst = list(set(lst)|set(lst_temp))
            file_lst = lst

        elif type(file_type) is str:
            file_lst = [x for x in file_lst if x.endswith('.' + file_type)]
            if drop_file_type:
                file_lst = [x.split('.' + file_type)[0] for x in file_lst]
    # ex. file_type = 'swc' or 'csv' or ['txt', 'csv', 'swc']


    for t in ['.localized', '.DS_Store']:
        if t in file_lst:
            file_lst.remove(t)
        else:
            pass

    file_lst.sort()

    return file_lst


########################################################################################################################
def list_sampling(lst, n=None, pct=None, with_replacement=False, sampling_times=10, shuffle_list=True, cover_all=True):
    '''
    :param lst: target list
    :param n: separate lst into n groups
    :param pct: separate lst into (1/pct) groups with (len(lst)*pct) in every group
    pct=0.672 -> test_set=154, train+cv=60+15
    pct=0.562 -> test_set=129, train+cv=80+20
    pct=0.452 -> test_set=104, train+cv=100+25
    pct=0.345 -> test_set=79, train+cv=120+30
    pct=0.126 -> test_set=29, train+cv=160+40
    '''

    if shuffle_list:
        random.shuffle(lst)

    sample_set = []
    remain_set = []
    if with_replacement:
        if all([n is None, pct > 0, pct < 1]):
            num = round(pct*len(lst))

            if cover_all:
                # make sure every element has been selected once
                if pct*sampling_times < 1:
                    sys.exit("\n 'sampling_times' is too small to 'cover_all_data'. Check list_sampling().")

                remain_times = sampling_times
                while True:
                    # evenly divide the target list into lists
                    random.shuffle(lst)
                    _sampling_lst = [lst[i:i + num] for i in range(0, len(lst), num)]
                    _l = _sampling_lst[-2] + _sampling_lst[-1]
                    _l = _l[-num:]
                    _sampling_lst = _sampling_lst[:-1]
                    _sampling_lst.append(_l)

                    # update sampling_lst and remain_time
                    if remain_times >= len(_sampling_lst):
                        remain_times -= len(_sampling_lst)
                        sample_set += _sampling_lst
                        if remain_times == 0:
                            break
                    else:
                        _sampling_lst = _sampling_lst[:remain_times]
                        sample_set += _sampling_lst
                        break


            else:
                sample_set = []
                for i in range(sampling_times):
                    x = random.sample(lst, num)
                    sample_set.append(x)

        else:
            sys.exit("\n 'with_replacement' and 'sampling_times' can be used only with 'Percent'(pct=0.1, 0.2, 0.3,...). Check list_sampling().")



    else:
        if all([n is not None, pct is None]):
            if n > len(lst):
                sys.exit("\n n > len(lst)! Check list_sampling().")
            else:
                division = len(lst) / n
        elif all([n is None, pct > 0, pct < 1]):
            val = 1 / pct
            n = round(val)
            division = len(lst) / n
        else:
            sys.exit("\n Use either Number(n=1, 2, 3...) or Percent(pct=0.1, 0.2, 0.3,...) to separate the list. Check list_sampling().")

        sample_set = [lst[round(division * i):round(division * (i + 1))] for i in range(n)]

    # remain_set
    for i in range(len(sample_set)):
        _lst = list(set(lst) - set(sample_set[i]))
        remain_set.append(_lst)

    return sample_set, remain_set


########################################################################################################################
def list_unique(mylist):
    x = np.array(mylist)
    x = list(np.unique(x))
    x.sort()
    return x


########################################################################################################################
def dict_merge_value(d, unique=True):
    lst = []
    for k, v in d.items():
        if lst is None:
            lst = v
        else:
            lst += v

    if unique:
        lst = list_unique(lst)

    return lst


########################################################################################################################
def dict_sampling(d, pct=None, num=None, sample_times=10, min=1):
    '''
    pct=0.87 -> sample_set=160+40
    pct=0.655 -> sample_set=120+30
    pct=0.54 -> sample_set=100+25
    pct=0.43 -> sample_set=80+20
    pct=0.325 -> sample_set=60+15
    '''
    sample_set = []
    remain_set = []
    total_lst = dict_merge_value(d)
    for i in range(sample_times):
        _lst = []
        for k, v in d.items():
            if len(v) <= min:
                _lst += v
            else:
                try:
                    if all([type(num) is int, pct is None]):
                        n = num
                    elif all([num is None, 0<pct<1]):
                        n = round(len(v)*pct)
                except:
                    sys.exit("\n Use num(int) or pct(0,1) correctly! Check dict_sampling().")

                random.shuffle(v)
                v = v[:n]
                _lst += v

        _r_lst = list(set(total_lst)-set(_lst))
        sample_set.append(_lst)
        remain_set.append(_r_lst)

    return sample_set, remain_set


########################################################################################################################
def pyramid(df, feature_list, num_layer, th_layer, for_train=False, empty_number=-1):
    """
        Give

        :param df:
            type: DataFrame
            only permit df of one neuron list (level tree)
        :param feature_list:
            type: list
            the features. For example: ['s', 'norm_s']
        :param num_layer:
            type: int
            the number of pyramid layer you want --> 2^num-1 nodes
        :param th_layer:
            type: int
            if the number of layers is less than this parameter, then it will be ignored.
        :param for_train:
            type: bool
            the pyramid is for training or not, i.e. with label or not

        :return:
            type: DataFrame
            for example, you give
                feature_list = ['s', 'norm_s']
                num_layer = 2
                th_layer = 1

                the output --> df["nrn", "ID", "T", "s_1", "norm_s_1", "s_2", "norm_s_2", "s_3", "norm_s_3"]
                "T" --> 0:dendrite, 1:axon
    """

    def index_searchv2(lst, value):
        index_p = -1
        for i in range(len(lst)):
            if lst[i][1] == value:
                index_p = i
                break

        return index_p

    def children_searchv2(lst, value):
        index_c = []
        for i in range(len(lst)):
            if lst[i][2] == value:
                index_c.append(i)

        return index_c

    def pyramid_structure(lst, n, layer, chosen_para, e_num):
        pyramid = []
        for i in range(layer):
            if i == 0:
                index = index_searchv2(lst, n)
                temp = [lst[index][l] for l in chosen_para]
                pyramid.append(temp)
                del temp
                anc = [n]
                continue

            temp_anc = []
            for j in anc:
                if j == -1:
                    temp_anc.append(e_num)
                    temp_anc.append(e_num)
                    pyramid.append([e_num for l in chosen_para])
                    pyramid.append([e_num for l in chosen_para])
                else:
                    index = children_searchv2(lst, j)
                    if len(index) >= 3:
                        change = True
                        while change:
                            change = False
                            for k in range(1, len(index)):
                                if lst[index[k - 1]][4] < lst[index[k]][4]:
                                    temp = index[k - 1]
                                    index[k - 1] = index[k]
                                    index[k] = temp
                                    change = True
                        for k in range(2):
                            temp_anc.append(lst[index[k]][1])
                        for k in range(2):
                            temp = [lst[index[k]][l] for l in chosen_para]
                            pyramid.append(temp)
                    if len(index) == 2:
                        if lst[index[0]][4] < lst[index[1]][4]:
                            temp = index[0]
                            index[0] = index[1]
                            index[1] = temp
                        for k in range(2):
                            temp_anc.append(lst[index[k]][1])
                        for k in range(2):
                            temp = [lst[index[k]][l] for l in chosen_para]
                            pyramid.append(temp)
                    if len(index) == 0:
                        temp_anc.append(e_num)
                        temp_anc.append(e_num)
                        pyramid.append([e_num for l in chosen_para])
                        pyramid.append([e_num for l in chosen_para])
            anc = temp_anc
        temp = pyramid[0]
        for i in range(1, len(pyramid)):
            for j in range(len(pyramid[i])):
                temp.append(pyramid[i][j])
        pyramid = temp
        del temp

        return pyramid

    def list_to_train_data(lst, num_layer, threshold_layer, para_list, file_name, chosen_para, train, e_num):
        # list --> train data
        # check the layer of pyramid
        nodes = []
        for i in range(len(lst)):
            if lst[i][5] == 2 or lst[i][5] == 3:
                nodes.append(i)
                continue
        result = []
        for i in nodes:
            fulfill = False
            index = children_searchv2(lst, lst[i][1])
            if len(index) == 0:
                count = 1
            else:
                count = 2
            while not fulfill:
                if count >= threshold_layer:
                    result.append(i)
                    break

                temp = []
                for j in index:
                    if lst[j][3] != 0:
                        for k in children_searchv2(lst, lst[j][1]):
                            temp.append(k)
                index = temp
                del temp

                if len(index) > 0:
                    count += 1
                else:
                    fulfill = True

        # create train data
        if len(result) == 0:
            return pd.DataFrame([])
        train_data = []
        label_data = []
        nrn = []
        for i in result:
            nrn.append([file_name, lst[i][1]])
            pyramid = pyramid_structure(lst, lst[i][1], num_layer, para_list, e_num)
            train_data.append(pyramid)
            if lst[i][5] == 2:
                label_data.append(1)  # Axon
            if lst[i][5] == 3:
                label_data.append(0)  # Dendirte
        nrn_t = list(map(list, zip(*nrn)))
        res_dict = {
            "nrn": nrn_t[0],
            "ID": nrn_t[1]
        }
        if train:
            res_dict["label"] = label_data

        train_data_t = list(map(list, zip(*train_data)))
        temp = 0
        col = []
        for i in range(len(train_data_t)):
            if i % len(chosen_para) == 0:
                temp += 1
            col.append(chosen_para[i % len(chosen_para)] + "_" + str(temp))
        for i in range(len(train_data_t)):
            res_dict[col[i]] = train_data_t[i]

        res_df = pd.DataFrame(res_dict)

        return res_df

    def create_train_data(lst, num_layer, threshold_layer, chosen_para, train, e_num):
        # change form of chosen_para
        para_list = [6 + i for i in range(len(chosen_para))]

        # check the list of neuron files
        file_names = []
        for i in range(len(lst)):
            if lst[i][0] not in file_names:
                file_names.append(lst[i][0])

        # create the right form of training data
        res_df = pd.DataFrame()
        start = 0

        # sort the neuron

        # pick one neuron
        for i in file_names:
            temp_file_name = i
            temp_lst = []
            touch = False
            for j in range(start, len(lst)):
                if lst[j][0] == i:
                    touch = True
                    temp_lst.append(lst[j])
                if touch:
                    if lst[j][0] != i:
                        # start = j
                        break

            temp_df = list_to_train_data(temp_lst, num_layer, threshold_layer,
                                         para_list, temp_file_name, chosen_para, train, e_num)
            res_df = pd.concat([res_df, temp_df], ignore_index=True)

        return res_df

    if for_train:
        df_p = df.loc[:, ['nrn', 'ID', 'PARENT_ID', 'NC', 'dp_level', 'type_pre'] + feature_list]
        df_lst = df_p.values.tolist()
        pyra = create_train_data(df_lst, num_layer, th_layer, feature_list, for_train, empty_number)
    else:
        df_p = df.loc[:, ['nrn', 'ID', 'PARENT_ID', 'NC', 'dp_level'] + feature_list]
        fk_lst = [2 for i in range(df_p.shape[0])]
        df_p.insert(loc=5, column='type_pre', value=fk_lst)
        df_lst = df_p.values.tolist()
        pyra = create_train_data(df_lst, num_layer, th_layer, feature_list, for_train, empty_number)

    return pyra


########################################################################################################################
def classification(train_df, test_df, label, features, model, standardization=False,
                   pred_threshold=None, cv=False):  # Info of classifiers.
    # y
    # check if there is a label col in test_df
    for col in label:
        if col in test_df.columns:
            y_test = np.array(test_df[label[0]])
        else:
            y_test = None
    y_train = np.array(train_df[label[0]])

    # x
    X_test = np.array(test_df[features])
    X_train = np.array(train_df[features])

    ### Run (w/ standardize)
    if standardization:
        scaler = preprocessing.StandardScaler().fit(X_train)
        model.fit(scaler.transform(X_train), y_train)

        ### Predict output: 1/0, threshold: default
        if pred_threshold is None:
            pred_proba = model.predict_proba(scaler.transform(X_test))
            y_pred = model.predict(scaler.transform(X_test))
            # y_proba = pred_proba[:, 1]

        ### Predict output: 1/0, threshold: float
        else:
            try:
                pred_proba = model.predict_proba(scaler.transform(X_test))
                y_pred = np.where(pred_proba[:, 1] >= pred_threshold, 1, 0)

            except:
                print('Invalid settings!')




                ## Simon
                # scaler = preprocessing.StandardScaler().fit(X_train.reshape(len(X_train), -1))
                # # scaler1 = preprocessing.StandardScaler().fit(X_train)
                # # a = (scaler.mean_ == scaler1.mean_)
                # # b = (scaler.std_ == scaler1.std_)
                # model.fit(scaler.transform(X_train.reshape(len(X_train), -1)), y_train)
                # y_pred = model.predict(scaler.transform(X_test.reshape(len(X_test), -1)))

    ### Run (w/o standardize)
    else:
        model.fit(X_train, y_train)

        ### Predict output: 1/0, threshold: default
        if pred_threshold is None:
            pred_proba = model.predict_proba(X_test)
            y_pred = model.predict(X_test)

        ### Predict output: 1/0, threshold: float
        else:
            try:
                pred_proba = model.predict_proba(X_test)
                y_pred = np.where(pred_proba[:, 1] >= pred_threshold, 1, 0)

            except:
                print('Invalid pred_threshold!')





                ## Simon
                # model.fit(X_train.reshape(len(X_train), -1), y_train)
                # y_pred = model.predict(X_test.reshape(len(X_test), -1))



    if cv is True:
        title = "Learning Curves "
        # SVC is more expensive so we do a lower number of CV iterations:
        cv = ShuffleSplit(n_splits=10, test_size=0.2, random_state=0)
        estimator = model
        plot_learning_curve(estimator, title, X_train, y_train, (0.7, 1.01), cv=cv, n_jobs=4)
    else:
        pass




    return y_test, y_pred, pred_proba, model

########################################################################################################################
def post_relabel(df, threshold=0.8, reduce_value=0.05):
    """
        Give the prediction after relabeling

        :param df:
            type: DataFrame
            df of all neurons (level tree)
            need to be sorted by nrn ID
            what you should add:
                nrn (type:str): each node should have a corresponding neuron
                PROBABILTY (type:list): probabilties (to be axon) predicted by machines
        :param threshold:
            type: float
            the threshold of classifying neuronal polarity

        :return:
            type: DataFrame
            the input data with a new column: type_post
    """

    def index_search(lst, value):
        if value == -1:
            return -1
        index = -1

        for i in range(len(lst)):
            if lst[i][1] == value:
                index = i
                break

        return index

    def children_search(lst, value):
        index = []
        for i in range(len(lst)):
            if lst[i][2] == value:
                index.append(i)
        return index

    def relabel_process(lst):
        for j in range(len(lst)):
            if lst[j][3] == 0 and lst[j][4] == 0:
                index = index_search(lst, lst[j][2])
                while True:
                    if lst[index][4] != 0:
                        lst[j][4] = lst[index][4]
                        break
                    index = index_search(lst, lst[index][2])
                    if index == -1:
                        break

        buffer_c = []
        buffer_p = []

        for j in range(len(lst)):
            if lst[j][3] == 0:
                buffer_c.append(j)

        while len(buffer_c) != 0:
            buffer_3 = []
            for j in buffer_c:
                buffer_p.append(index_search(lst, lst[j][2]))
            buffer = set(buffer_p)
            for j in buffer:
                if buffer_p.count(j) == lst[j][3]:

                    location = [k for k, v in enumerate(buffer_p) if v == j]
                    buffer_3 = buffer_3 + location
                    buffer_c.append(j)

                    buffer_2 = []
                    for k in range(len(location)):
                        buffer_2.append(lst[buffer_c[location[k]]][4])

                    dendrite_count = buffer_2.count(3)
                    axon_count = buffer_2.count(2)
                    mix_count = buffer_2.count(4)

                    if mix_count > 1:
                        result = 4
                    else:
                        if dendrite_count != 0 and axon_count == 0:
                            result = 3
                        elif dendrite_count == 0 and axon_count != 0:
                            result = 2
                        elif dendrite_count != 0 and axon_count != 0:
                            result = 4
                        else:
                            result = 0

                    lst[j][4] = result

            buffer_3.sort()
            buffer_3.reverse()
            for k in buffer_3:
                buffer_c.pop(k)
            if 0 in buffer_c:
                location = [k for k, v in enumerate(buffer_c) if v == 0]
                location.reverse()
                for k in location:
                    buffer_c.pop(k)
            buffer_p = []

        for j in range(len(lst)):
            c_index = children_search(lst, lst[j][1])
            for k in c_index:
                if lst[k][-1] == 0:
                    lst[k][-1] = lst[j][-1]
        return lst

    def threshold_process(lst, th):
        for j in range(len(lst)):
            if math.isnan(lst[j][4]):
                lst[j].pop(-1)
                lst[j].append(0)
                continue

            if lst[j][4] >= th:
                lst[j][4] = 2
            elif lst[j][4] < 1 - th:
                lst[j][4] = 3
            else:
                lst[j][4] = 0
        return lst

    # load information
    df.sort_values(by=['nrn', 'ID'])
    df_p = df.loc[:, ['nrn', 'ID', 'PARENT_ID', 'NC', 'prob']]
    df_lst = df_p.values.tolist()

    # neuron name
    file_name = []
    point_s = []
    for i in range(len(df_lst)):
        if df_lst[i][0] not in file_name:
            point_s.append(i)
            file_name.append(df_lst[i][0])

    for i in range(len(file_name)):

        # filter 1: before relabel
        threshold_i = threshold
        while threshold_i != 0.5:
            dendrite_like = False
            axon_like = False
            for j in range(point_s[i], len(df_lst)):
                if df_lst[j][0] != file_name[i]:
                    break
                if df_lst[j][4] >= threshold_i:
                    axon_like = True
                elif df_lst[j][4] < 1 - threshold_i:
                    dendrite_like = True
                if axon_like and dendrite_like:
                    break
            if axon_like and dendrite_like:
                break
            else:
                threshold_i = threshold_i - reduce_value
                if threshold_i > 0.5:
                    pass
                else:
                    threshold_i = 0.5

        # replacing by the method of terminals
        lst1 = []
        for j in range(point_s[i], len(df_lst)):
            if df_lst[j][0] != file_name[i]:
                break
            lst1.append(df_lst[j])

        # Calculate
        lst2 = threshold_process(lst1, threshold_i)
        lst2 = relabel_process(lst2)

    #filter 2: after relabel
        while threshold_i != 0.5:
            dendrite_like = False
            axon_like = False
            for j in range(len(lst2)):
                if lst2[j][4] == 2:
                    axon_like = True
                elif lst2[j][4] == 3:
                    dendrite_like = True
            if axon_like and dendrite_like:
                break
            else:
                threshold_i -= reduce_value
                if threshold_i > 0.5:
                    lst2 = threshold_process(lst1, threshold_i)
                    lst2 = relabel_process(lst2)
                else:
                    threshold_i = 0.5
                    lst2 = threshold_process(lst1, threshold_i)
                    lst2 = relabel_process(lst2)

        for j in range(len(lst2)):
            df_lst[point_s[i] + j][4] = lst2[j][4]


    # check if both axon and dendrite exist
    bad_case = []
    for i in range(len(file_name)):
        lst = []
        for j in range(point_s[i], len(df_lst)):
            if df_lst[j][0] != file_name[i]:
                break
            lst.append(df_lst[j])

        dendrite_like = False
        axon_like = False
        for j in range(len(lst)):
            if df_lst[j][4] == 2:
                axon_like = True
            elif df_lst[j][4] == 3:
                dendrite_like = True
        if axon_like and dendrite_like:
            pass
        else:
            bad_case.append(i)

    lst = ""
    if len(bad_case) == 0:
        lst = "None"
    else:
        lst = lst + file_name[bad_case[0]]
        for i in range(1, len(bad_case)):
            lst += " " + file_name[bad_case[i]]
    print("still bad : ", lst)

    res = []
    for i in range(len(df_lst)):
        res.append(df_lst[i][4])
    df["type_post"] = res

    return df


########################################################################################################################
def print_info_dict(dict):
    print("info_dict:")
    for y in dict:
        print("\t", y, ':', dict[y])

    return


########################################################################################################################
def turn_confusingMatrix_to_DF(cm, post_relabel=True):
    if post_relabel:
        lst = ['axon', 'dend']
    else:
        lst = ['axon', 'other']
    df = pd.DataFrame(cm, index=lst, columns=lst)
    return df


########################################################################################################################
def plot_tree_typePost(df, child_col, parent_col, label_col, pred_col, save_path=None, file_type='pdf', fig_size='6,6', only_terminal=True, branch_col="l", show_node_id=True, view=False, delete_gv_files=True):
    # Create total nodes
    df = df.sort_values(["dp_level", child_col], ascending=[False, True]).reset_index(drop=True)
    total_node = [1] + df.loc[:, child_col].tolist()

    if label_col is not None:
        df[label_col] = np.where(df[label_col]==0, np.nan, df[label_col])
    if only_terminal:
        df[pred_col] = np.where(df["NC"]!=0, np.nan, df[pred_col])

    # Plotting
    # 1. file name
    filename = df.loc[0, "nrn"]
    if label_col is None:
        filename = "pre_" + filename
    u = Digraph(filename, format=file_type)
    u.attr(size=fig_size, bgcolor='transparent' )

    # 2. plot nodes
    dict0 = {1: 'black', 2: '#1E90FF', 3: '#ff1e1e', 4: '#f8ba00', 0: 'gray'}  # fill in
    # dict0 = {2: 'palegreen1', 3: 'lightpink', 4: 'plum', 1:'black'}  # fill in
    # dict1 = {2: 'green4', 3: 'red', 4: 'purple', 1:'black'}    # shape
    dict1 = {2: 'black', 3: 'black', 4: 'black', 1: 'black'}  # shape
    for node in total_node:
        if show_node_id:
            node_id = str(node)
        else:
            node_id = ""

        # 2.1 assign type to soma
        if node == 1:
            type_true = type_pred = 1
        elif label_col is None:
            type_pred = df.loc[df[child_col] == node, pred_col].values[0]
        else:
            type_true = df.loc[df[child_col]==node, label_col].values[0]
            type_pred = df.loc[df[child_col]==node, pred_col].values[0]

        # 2.2 color the nodes
        if label_col is None:   # WITHOUT label column
            if node == 1:
                u.attr('node', shape='circle', color='black', penwidth='1', style='filled', fillcolor=dict0[type_pred])
                u.node(str(node), label=node_id)
            elif math.isnan(type_pred):
                u.attr('node', shape='circle', color='black', penwidth='1', style='solid')
                u.node(str(node), label=node_id)
            else:
                u.attr('node', shape='circle', color=dict0[type_pred], penwidth='1', style='filled', fillcolor=dict0[type_pred])
                u.node(str(node), label=node_id)
            del type_pred

        else:   # label column
            if node == 1:
                u.attr('node', shape='circle', color=dict1[type_true], penwidth='1', style='filled',
                       fillcolor=dict0[type_pred])
                u.node(str(node), label=node_id)
            elif any([math.isnan(type_true), math.isnan(type_pred)]):
                u.attr('node', shape='circle', color='black', penwidth='1', style='solid')
                u.node(str(node), label=node_id)
            elif type_true == type_pred:
                u.attr('node', shape='circle', color=dict0[type_true], penwidth='1', style='filled',
                       fillcolor=dict0[type_pred])
                u.node(str(node), label=node_id)
            else:
                u.attr('node', shape='circle', color=dict1[type_true], penwidth='5', style='filled',
                       fillcolor=dict0[type_pred])
                u.node(str(node), label=node_id)
            del type_true, type_pred


    # 3. plot the link btw nodes (show distance on the link)
    if branch_col is None:
        for index, row in df.iterrows():
            child = str(row[child_col])
            parent = str(row[parent_col])
            u.edge(parent, child, arrowhead="none")

    else:
        for index, row in df.iterrows():
            child = str(row[child_col])
            parent = str(row[parent_col])
            label = str(int(round(row[branch_col], 0)))
            u.edge(parent, child, label=label, arrowhead="none")


    # View/save the file
    if view:
        main_folder_name = 'gv_graphs '
        if not os.path.exists(Desktop + main_folder_name):
            try:
                os.makedirs(Desktop + '/' + main_folder_name)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

        u.view(directory=Desktop + main_folder_name)
        path = Desktop + main_folder_name


    elif all([view is False, save_path is None]):
        main_folder_name = 'gv_graphs '
        if not os.path.exists(Desktop + main_folder_name):
            try:
                os.makedirs(Desktop + '/' + main_folder_name)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

        u.render(directory=Desktop + main_folder_name, view=False)
        path = Desktop + main_folder_name


    elif all([view is False, save_path is not None]):
        u.render(directory=save_path, view=False)
        path = save_path


    if delete_gv_files:
        delete_files_under_directory(directory=path, file_type='gv')

    return


########################################################################################################################
def delete_files_under_directory(directory='/Users/csu/Desktop/Neuron/nrn_plot', file_type='gv'):
    file_lst = []
    if directory.endswith("/"):
        directory = directory[:-1]

    for (dirpath, dirnames, filenames) in os.walk(directory):
        file_lst.extend(filenames)
        break

    if all([file_type is not None, file_type != 'all']):
        if type(file_type) is list:
            lst = []
            for t in file_type:
                lst_temp = [x for x in file_lst if x.endswith('.' + t)]
                lst = list(set(lst)|set(lst_temp))
            file_lst = lst

        elif type(file_type) is str:
            file_lst = [x for x in file_lst if x.endswith('.' + file_type)]
    # ex. file_type = 'swc' or 'csv' or ['txt', 'csv', 'swc']


    for file in file_lst:
        os.remove(directory + '/' + file)

    return


########################################################################################################################
def neuron_plot_relation_tree(df, child_col, parent_col, branch_col=None, polarity_dict=None, node_order_lst=None,
                              show_node_id=True, view=False, save_path=None, filename='ntree', file_type='pdf',
                              fig_size='6,6', delete_gv_files=True):

    ### Need to drop row that "PARENT_ID = -1" first
    df = df[df[parent_col] != -1]

    u = Digraph(filename, format=file_type)
    u.attr(size=fig_size, bgcolor='transparent')


    ### Create total nodes
    if node_order_lst is None:
        total_node = list(set(df[child_col]) | set(df[parent_col]))
    else:
        node_from_df = list(set(df[child_col]) | set(df[parent_col]))
        if set(node_from_df).issubset(node_order_lst):
            total_node = [x for x in node_order_lst if x in node_from_df]
        else:
            sys.exit("\n node_from_df is not a sub-set of node_order_lst! Check neuron_plot_relation_tree().")


    ### Plot nodes
    dict0 = {1: 'black', 2: '#1E90FF', 3: '#ff1e1e', 4: '#f8ba00', 0: 'gray'}
    # dict0 = {2: 'green4', 3: 'red', 4: 'purple'}
    # dict0 = {2: 'palegreen1', 3: 'lightpink', 4: 'plum'}  # fill in
    if polarity_dict is None:
        u.attr(size=fig_size)
        u.node_attr.update(shape='circle', color='black', style='solid')
    else:
        ### Add type col
        df['type'] = np.where(df[child_col].isin(polarity_dict['axon']), 2, 0)
        df['type'] = np.where(df[child_col].isin(polarity_dict['dendrite']), 3, df['type'])
        df['type'] = np.where(df[child_col].isin(polarity_dict['mix']), 4, df['type'])

        for node in total_node:
            if node != 1:
                t = df.loc[df[child_col]==node, 'type'].values[0]

            if show_node_id:
                node_id = str(node)
            else:
                node_id = ""

            if node == 1:
                u.attr('node', shape='circle', color='black', style='filled')
                u.node(str(node), label=node_id)
            elif t in [2,3,4]:
                u.attr('node', shape='circle', color=dict0[t], style='filled')
                u.node(str(node), label=node_id)
            else:
                u.attr('node', shape='circle', color='black', style='solid')
                u.node(str(node), label=node_id)


    ### Plot the link btw nodes
    if branch_col is None:
        for index, row in df.iterrows():
            child = str(row[child_col])
            parent = str(row[parent_col])
            u.edge(parent, child, arrowhead="none")

    else:
        for index, row in df.iterrows():
            child = str(row[child_col])
            parent = str(row[parent_col])
            label = str(int(round(row[branch_col], 0)))
            u.edge(parent, child, label=label, arrowhead="none")


    ### View/save the file
    if view:
        # main_folder_name = 'gv_graphs ' + datetime.date.today().strftime('%Y-%m-%d')
        main_folder_name = 'gv_graphs '
        if not os.path.exists(Desktop + main_folder_name):
            try:
                os.makedirs(Desktop + '/' + main_folder_name)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

        u.view(directory=Desktop + main_folder_name)
        path = Desktop + main_folder_name


    elif all([view is False, save_path is None]):
        main_folder_name = 'gv_graphs '
        if not os.path.exists(Desktop + main_folder_name):
            try:
                os.makedirs(Desktop + '/' + main_folder_name)

            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

        u.render(directory=Desktop + main_folder_name, view=False)
        path = Desktop + main_folder_name


    elif all([view is False, save_path is not None]):
        u.render(directory=save_path, view=False)
        # u.render("pdf", "pdf", save_path)
        path = save_path


    if delete_gv_files:
        delete_files_under_directory(directory=path, file_type='gv')

    return


########################################################################################################################
def neuron_path(df_path, des_point, anc_point, include_anc_pt=True):
    ### Create path list
    try:
        start = des_point
        end = anc_point

        path_points = df_path.loc[df_path['descendant'] == start, 'path'].tolist()

        start_idx = path_points.index(start)
        end_idx = path_points.index(end)

        if include_anc_pt:
            path_points = path_points[start_idx: (end_idx + 1)]
        else:
            path_points = path_points[start_idx: end_idx]

        return path_points

    except:
        print('No path between the two points. Check neuron_path().')


########################################################################################################################
def neuron_plot_remained_tree(df_dis_origin, df_dis_remained, child_col, parent_col, branch_col=None, polarity_dict=None,
                              node_order_lst=None, show_node_id=True, view=False, save_path=None, filename='rmn_tree',
                              file_type='pdf', fig_size='6,6', delete_gv_files=True):

    ### Need to drop row that "PARENT_ID = -1" first
    df = df_dis_origin[df_dis_origin[parent_col] != -1]
    df_temp = df_dis_remained[df_dis_remained[parent_col] != -1]

    # Create total nodes
    if node_order_lst is None:
        total_node = list(set(df[child_col]) | set(df[parent_col]))
    else:
        node_from_df = list(set(df[child_col]) | set(df[parent_col]))
        if set(node_from_df).issubset(node_order_lst):
            total_node = [x for x in node_order_lst if x in node_from_df]
        else:
            sys.exit("\n node_from_df is not a sub-set of node_order_lst! Check neuron_plot_remained_tree().")

    # Create remain nodes
    _, df_path = neuron_ancestors_and_path(df_dis_origin, child_col, parent_col)
    remain_node = []
    for index, row in df_temp.iterrows():
        des_point = row[child_col]
        anc_point = row[parent_col]
        path_points = neuron_path(df_path, des_point, anc_point)
        remain_node = list(set(remain_node) | set(path_points))


    ### Plotting
    # file name & file type
    u = Digraph(filename, format=file_type)

    u.attr(size=fig_size)

    # plot nodes
    dict0 = {1: 'black', 2: '#1E90FF', 3: '#ff1e1e', 4: '#f8ba00', 0: 'gray'}
    if polarity_dict is None:
        for node in total_node:
            # deleted nodes
            if node not in remain_node:
                u.attr('node', shape='circle', color='black', style='dashed')
                u.node(str(node))

            # remained nodes
            else:
                u.attr('node', shape='circle', color='black', style='solid')
                u.node(str(node))

    else:
        ### Add type col
        df['type'] = np.where(df[child_col].isin(polarity_dict['axon']), 2, 0)
        df['type'] = np.where(df[child_col].isin(polarity_dict['dendrite']), 3, df['type'])
        df['type'] = np.where(df[child_col].isin(polarity_dict['mix']), 4, df['type'])

        for node in total_node:
            if node != 1:
                t = df.loc[df[child_col] == node, 'type'].values[0]

            if show_node_id:
                node_id = str(node)
            else:
                node_id = ""

            if node == 1:
                u.attr('node', shape='circle', color='black', style='filled')
                u.node(str(node), label=node_id)
            # deleted nodes
            elif node not in remain_node:
                if t in [2, 3, 4]:
                    u.attr('node', shape='circle', color=dict0[t], style='dashed')
                    u.node(str(node), label=node_id)
                else:
                    u.attr('node', shape='circle', color='black', style='dashed')
                    u.node(str(node), label=node_id)
            # remained nodes
            else:
                if t in [2, 3, 4]:
                    u.attr('node', shape='circle', color=dict0[t], style='filled')
                    u.node(str(node), label=node_id)
                else:
                    u.attr('node', shape='circle', color='black', style='solid')
                    u.node(str(node), label=node_id)



    # plot the link btw nodes
    if branch_col is None:
        # don't show distance on the link
        for index, row in df.iterrows():
            child = str(row[child_col])
            parent = str(row[parent_col])
            u.edge(parent, child, arrowhead="none")

    else:
        # show distance on the link
        for index, row in df.iterrows():
            child = str(row[child_col])
            parent = str(row[parent_col])
            label = str(int(round(row[branch_col], 0)))
            u.edge(parent, child, label=label, arrowhead="none")


    ### View/save the file
    if view:
        # main_folder_name = 'gv_graphs ' + datetime.date.today().strftime('%Y-%m-%d')
        main_folder_name = 'gv_graphs '
        if not os.path.exists(Desktop + main_folder_name):
            try:
                os.makedirs(Desktop + '/' + main_folder_name)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

        u.view(directory=Desktop + main_folder_name)
        path = Desktop + main_folder_name


    elif all([view is False, save_path is None]):
        main_folder_name = 'gv_graphs '
        if not os.path.exists(Desktop + main_folder_name):
            try:
                os.makedirs(Desktop + '/' + main_folder_name)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

        u.render(directory=Desktop + main_folder_name, view=False)
        path = Desktop + main_folder_name


    elif all([view is False, save_path is not None]):
        u.render(directory=save_path, view=False)
        path = save_path


    if delete_gv_files:
        delete_files_under_directory(directory=path, file_type='gv')

    return


########################################################################################################################

