
########################################################################################################################
from util import *
from nrn_params import *
from settings import *
from nrn3_class_Prepare_Axon import Prepare_Axon
from nrn3_class_Data_Cleaner import Data_Cleaner
from nrn3_class_Create_Axon_Feature import Create_Axon_Feature



########################################################################################################################
# Set up
########################################################################################################################
input_folder = './data/'
# data
train_nrn_type = ["new_all_3"]
test_nrn_type = ["new_all_3"]
remove_method = None    # None or "leaf"
target_level = None     # None or int

# model
models = ["xgb"]      # "svm", "gbdt", "rf", "ridge", "xgb"
mdl_ensemble = 1                 # int >= 1
features = ['s', 'ns', 'ds', 'nds', 'l', 'nl', 'c1', 'as', 'rc']
pyramid_layer = 1       # int >= 1, 1: only target node, 2: target node + 1 generation, 3: target node + 2 generation.
threshold_layer = 1     # int >= 1, 1: need no descendants, 2: need at least 1 generation
sample = "region"   # "random" or "region"
sample_pct = 0.585    # (0, 1) or None: use all "train_nrn_type" data to train
sample_with_replacement = 20    # int >= 1

# other
overwrite = False    # True: overwrite the predict result of classify_data(), evaluation of evaluate()
rerun_mdl = False    # True: not use trained model to predict
relabel = True       # True: use post_relabel to evaluate
show = True          # True: show evaluation result

# plot
only_terminal=True   # True: plot terminal only
branch_col="l"       # None: show no branch length; "l": show branch length
show_node_id=True    # True: plot nodes with id number
ground_true=False


########################################################################################################################
# Main Code
########################################################################################################################
class Classify_Axon:

    def __init__(self,
                 input_folder,
                 train_nrn_type,
                 test_nrn_type,
                 remove_method=None,
                 target_level=None,
                 models=["xgb", "dnn"],
                 mdl_ensemble=1,
                 features=['l', 'nl', 's', 'ns', 'ds', 'nds', 'ro', 'c', 'rc'],
                 pyramid_layer=6,
                 threshold_layer=4,
                 sample="random",
                 sample_pct=None,
                 sample_with_replacement=10,
                 child_col='ID',
                 parent_col="PARENT_ID",
                 type_col='type_pre',   # Here use pre_relabel type 'type_pre'; the original type is 'T'.
                 overwrite=False
                 ):

        self.axon_classified_dict = {}
        self.classify_target = "axon"
        self.input_folder = input_folder
        self.train_nrn_type = sorted(train_nrn_type)
        self.test_nrn_type = sorted(test_nrn_type)
        self.remove_method = remove_method
        self.target_level = target_level
        self.features = sorted(features)
        self.pyr_layer = pyramid_layer
        self.th_layer = threshold_layer
        self.sample = sample
        self.sample_pct = sample_pct
        self.with_replacement = sample_with_replacement
        self.child_col = child_col
        self.parent_col = parent_col
        self.type_col = type_col
        self.overwrite = overwrite


        # Other configs
        self.branch = 2     # remove redundant branch (> 2)
        if sample_pct is not None:
            # select a portion of data to test set(when use random) or train set(use region)
            self.sample_pct = sample_pct


        # train & test nrn lst
        self.train_nrn_lst = []
        self.test_nrn_lst = []
        if self.train_nrn_type == self.test_nrn_type:
            for t in self.train_nrn_type:
                self.train_nrn_lst += neuron_dict[t]
            self.test_nrn_lst = self.train_nrn_lst
        else:
            for t in self.train_nrn_type:
                self.train_nrn_lst += neuron_dict[t]
            for t in self.test_nrn_type:
                self.test_nrn_lst += neuron_dict[t]
        self.train_nrn_lst = sorted(self.train_nrn_lst)
        self.test_nrn_lst = sorted(self.test_nrn_lst)


        # test & train samples
        if set(self.train_nrn_lst).isdisjoint(set(self.test_nrn_lst)):
            self.test_set = [self.test_nrn_lst for i in range(self.with_replacement)]
            self.train_set = [self.train_nrn_lst for i in range(self.with_replacement)]
            # self.with_replacement = None

        elif all([set(self.test_nrn_lst).issubset(set(self.train_nrn_lst)), self.train_nrn_type!=self.test_nrn_type]):
            self.train_nrn_lst = set(self.train_nrn_lst) - set(self.test_nrn_lst)
            self.test_set = [self.test_nrn_lst for i in range(self.with_replacement)]
            self.train_set = [self.train_nrn_lst for i in range(self.with_replacement)]

        elif sample == "random":
            if all([self.train_nrn_type == self.test_nrn_type, type(self.with_replacement) is int]):
                self.test_set, self.train_set = list_sampling(self.train_nrn_lst, pct=self.sample_pct, with_replacement=True, sampling_times=self.with_replacement)


        elif sample == "region":
            # dict samples
            if all([self.train_nrn_type == self.test_nrn_type, type(self.with_replacement) is int]):
                self.train_set, self.test_set = dict_sampling(group_dict, pct=self.sample_pct, sample_times=self.with_replacement, min=1)


        self.axon_classified_dict["sample_test_set"] = self.test_set
        self.axon_classified_dict["sample_train_set"] = self.train_set



        # fname
        _fname0 = "_".join(["result", self.classify_target])

        # _trainTest
        if self.train_nrn_type == self.test_nrn_type:
            _train = _test = _trainTest = '&'.join(self.train_nrn_type)
            _fname0 = "_".join([_fname0, _trainTest])
        else:
            _train = '&'.join(self.train_nrn_type)
            _test = '&'.join(self.test_nrn_type)
            _trainTest = "_".join([_train, _test])
            _fname0 = "_".join([_fname0, _trainTest])

        # _methodLevel
        if all([type(remove_method) is str, type(target_level) is int]):
            _methodLevel = remove_method + str(target_level)
            _fname0 = "_".join([_fname0, _methodLevel])

        # _features
        _features = "-".join(self.features)
        _fname0 = "_".join([_fname0, _features])

        # _pyr_th
        _pyr_th = "".join(["pyr", str(self.pyr_layer), "th", str(self.th_layer)])
        _fname0 = "_".join([_fname0, _pyr_th])

        # _sample_time_pct
        if type(self.with_replacement) is int:
            _sample_time_pct = sample + str(self.with_replacement) + "pct" + str(self.sample_pct)
            _fname0 = "_".join([_fname0, _sample_time_pct])


        # result fname
        self.fname_dict = {}
        self.mdl_ensemble = {}
        for m in models:
            # _mdl_ensemble (dnn model only)
            if all([m=="dnn", mdl_ensemble > 1]):
                self.mdl_ensemble[m] = mdl_ensemble
                _mdl_ensemble = "".join(["ens", str(mdl_ensemble)])
                _m = "_".join([m, _mdl_ensemble])
            elif mdl_ensemble > 1:
                print("\n Now only dnn can be used with ensemble. Mld_ensemble of other model will be set as default 1.")
                self.mdl_ensemble[m] = 1
                _m = m
            else:
                self.mdl_ensemble[m] = 1
                _m = m

            self.fname_dict[m] = input_folder + "nrn_result/" + "_".join([_fname0, _m]) + ".pkl"


        # model fname
        self.model_fname_dict = {}
        for m in models:
            self.model_fname_dict[m] = {}
            mfname = _fname0.replace("result", "model")
            mfname = "_".join([mfname, m])
            if all([self.train_nrn_type == self.test_nrn_type, type(self.with_replacement) is int]):
                for i in range(self.with_replacement):
                    self.model_fname_dict[m][i] = input_folder + "nrn_model/" + "_".join([mfname, str(i)])
            else:
                self.model_fname_dict[m][0] = input_folder + "nrn_model/" + "_".join([mfname, str(0)])

        # trained model fname
        self.trained_model_fname_dict = {}
        for m in models:
            self.trained_model_fname_dict[m] = {}
            mfname = _fname0.replace("result", "model")
            mfname = mfname.replace(_trainTest, _train)
            mfname = "_".join([mfname, m])
            if type(self.with_replacement) is int:
                for i in range(self.with_replacement):
                    self.trained_model_fname_dict[m][i] = input_folder + "nrn_model/" + "_".join([mfname, str(i)])
            else:
                self.trained_model_fname_dict[m][0] = input_folder + "nrn_model/" + "_".join([mfname, str(0)])


        # Models
        gbdt_class = ensemble.GradientBoostingClassifier()
        rf_class = ensemble.RandomForestClassifier()
        svm_class = SVC(kernel='rbf', class_weight='balanced', probability=True, gamma="scale")
        xgb_class = XGBClassifier(seed=123)

        self.models = {'gbdt': gbdt_class, 'rf': rf_class, 'svm': svm_class, 'xgb': xgb_class}
        self.models = {k: self.models[k] for k in models}

        return



    def classify_data(self, rerun_mdl=False):
        self.rerun_mdl = rerun_mdl
        if all([self._is_ready(), not self.overwrite]):
            self._load_data()
        else:
            self._prepare_data_from_Create_Axon_Feature()
            self._run_model()
            self._load_data()
        return self


    def predict(self, prob_threshold=None):
        self.prob_threshold = prob_threshold
        if all([self._is_ready(), not self.overwrite]):
            self._load_data()
        else:
            self._prepare_data_from_Create_Axon_Feature()
            self._predict(prob_threshold)
            self._load_data()
        return self


    def evaluate(self, relabel=True, show=True, prob_thresholds=np.arange(0.5, 1, 0.05)):
        self.post_relabel = relabel
        self.prob_thresholds = prob_thresholds
        if not self.post_relabel:
            self.prob_thresholds = [0.5]    # if not post relabel, pred threshold will fixed at 0.5
        if not self._is_ready():
            self.classify_data()
        self._load_data()
        self._data_info()
        self._evaluate()
        if show:
            self._show_evaluation()
        return


    def plot_result(self, prob_threshold, file_type, file_size='20, 20', only_terminal=True, branch_col="l", show_node_id=True, ground_truth=True):
        self.prob_threshold = prob_threshold
        self.file_type = file_type
        self.file_size = file_size
        self.only_terminal = only_terminal
        self.branch_col = branch_col
        self.show_node_id = show_node_id
        self.ground_truth = ground_truth
        if not self._is_ready():
            self.classify_data()
        self._load_data()
        self._plot_result()
        return


    def load_data(self):
        if all([self._is_ready(), not self.overwrite]):
            self._load_data()
        return self.axon_classified_dict


    def _is_ready(self):
        for _, fname in self.fname_dict.items():
            if os.path.exists(fname):
                x = True
            else:
                x = False
                break
        return x


    def _load_data(self):
        self.axon_classified_dict = {}
        for k, v in self.fname_dict.items():
            with open(v, "rb") as file:
                self.axon_classified_dict[k] = pickle.load(file)
        return


    def _prepare_data_from_Create_Axon_Feature(self):
        print("Prepare Data...")
        # train
        ax0 = Prepare_Axon(self.input_folder, self.train_nrn_type, self.remove_method, self.target_level)
        ax0 = ax0.load_data()
        df = ax0.prepare_dict["forTrain"]
        _df = df.copy()
        del ax0

        # test
        if self.train_nrn_type != self.test_nrn_type:
            ax0 = Prepare_Axon(self.input_folder, self.test_nrn_type, self.remove_method, self.target_level)
            ax0 = ax0.load_data()
            df = ax0.prepare_dict["forTrain"]
            del ax0

        gc.collect()

        self.test = pyramid(df, self.features, self.pyr_layer, self.th_layer, for_train=False, empty_number=-1)
        self.train = pyramid(_df, self.features, self.pyr_layer, self.th_layer, for_train=True, empty_number=-1)

        return


    def _run_model(self):
        print("Run Axon Models...")

        feature_lst = []
        for j in self.features:
            for k in range(2 ** self.pyr_layer - 1):
                feature_lst.append(j + "_" + str(k + 1))


        bar = progressbar.ProgressBar()
        time.sleep(0.01)
        for model_name, model in bar(self.models.items()):
            # check if result exist
            if all([os.path.exists(self.fname_dict[model_name]), not self.overwrite]):
                continue

            if self.mdl_ensemble[model_name] == 1:
                random.seed(123)

            if model_name not in ('svm', 'dnn'):
                df_ftr_rank = pd.DataFrame(index=feature_lst)

            test_df3 = None
            for idx0 in range(self.with_replacement):
                # test df
                test_df = self.test.loc[self.test['nrn'].isin(self.test_set[idx0])].reset_index(drop=True)

                # train df
                _nrn_lst = self.train_set[idx0]
                train_df = self.train.loc[self.train['nrn'].isin(_nrn_lst)].reset_index(drop=True)


                # run model
                for e in range(self.mdl_ensemble[model_name]):
                    # find save path
                    if model_name == "dnn":
                        sp = self.model_fname_dict[model_name][idx0] + "-" + str(e) + ".pkl"
                    elif model_name == "xgb":
                        sp = self.model_fname_dict[model_name][idx0] + "-" + str(e) + ".pkl"

                    # use exist models to predict
                    if all([os.path.exists(sp), not self.rerun_mdl]):
                        if model_name == "dnn":
                            model1.save_dir(sp)
                        elif model_name == "xgb":
                            with open(sp, "rb") as file:
                                model1 = pickle.load(file)
                        pred_proba = model1.predict_proba(np.array(test_df[feature_lst]))
                    # run new models
                    else:
                        if model_name == "dnn":
                            model.save_dir(sp)
                        y_test, y_pred, pred_proba, model1 = classification(train_df, test_df, label=['label'], features=feature_lst, model=model)
                        if model_name =="xgb":  # save xgb only
                            with open(sp, "wb") as file:
                                pickle.dump(model1, file=file)

                    test_df2 = test_df[['nrn', 'ID']].copy()
                    if model_name == "dnn":
                        test_df2['prob'] = pred_proba
                    else:
                        test_df2['prob'] = pred_proba[:, 1]

                    if test_df3 is None:
                        test_df3 = test_df2
                    else:
                        test_df3 = test_df3.append(test_df2)

                    # Add feature ranking columns
                    if model_name not in ('svm', 'dnn'):
                        df_ftr_rank[idx0] = model1.feature_importances_

                    del model1, sp

            # Find top 10 features
            if model_name not in ('svm', 'dnn'):
                df_ftr_rank['average'] = df_ftr_rank.mean(axis=1)
                df_ftr_rank = df_ftr_rank[['average']]
                df_ftr_rank = df_ftr_rank.sort_values(['average'], ascending=False).reset_index()
                df_ftr_rank = df_ftr_rank.head(10)
                # print(df_ftr_rank)

            test_df3 = test_df3.groupby(["nrn", self.child_col], as_index=False)["prob"].mean()
            self.axon_classified_dict["avg_result"] = test_df3.sort_values(['nrn', 'ID']).reset_index(drop=True)

            # Add ftr ranking
            if model_name not in ('svm', 'dnn'):
                self.axon_classified_dict["ftr_ranking"] = df_ftr_rank

            # save result
            with open(self.fname_dict[model_name], "wb") as file:
                pickle.dump(self.axon_classified_dict, file=file)

        time.sleep(0.01)

        return


    def _predict(self, prob_threshold):
        print("Predict...")

        feature_lst = []
        for j in self.features:
            for k in range(2 ** self.pyr_layer - 1):
                feature_lst.append(j + "_" + str(k + 1))

        bar = progressbar.ProgressBar()
        time.sleep(0.01)
        for model_name, _ in bar(self.models.items()):
            # check if result exist
            if all([os.path.exists(self.fname_dict[model_name]), not self.overwrite]):
                continue

            test_df3 = None
            for idx0 in range(self.with_replacement):
                # test df
                test_df = self.test.loc[self.test['nrn'].isin(self.test_set[idx0])].reset_index(drop=True)

                # run model
                for e in range(self.mdl_ensemble[model_name]):
                    # find saved model path
                    if model_name == "dnn":
                        sp = "./data/nrn_model/DNN/"
                    if model_name == "xgb":
                        sp = self.trained_model_fname_dict[model_name][idx0] + "-" + str(e) + ".pkl"

                    # use exist models to predict
                    if os.path.exists(sp):
                        if model_name == "xgb":
                            with open(sp, "rb") as file:
                                model1 = pickle.load(file)
                            print(np.array(test_df[feature_lst])[0:5])
                            pred_proba = model1.predict_proba(np.array(test_df[feature_lst]))
                            del model1
                    else:
                        print("No trained model available for prediction.")


                    test_df2 = test_df[['nrn', 'ID']].copy()
                    if model_name == "dnn":
                        test_df2['prob'] = pred_proba
                        test_df3 = test_df2
                    else:
                        test_df2['prob'] = pred_proba[:, 1]
                        if test_df3 is None:
                            test_df3 = test_df2
                        else:
                            test_df3 = test_df3.append(test_df2)

                    del sp


            test_df3 = test_df3.groupby(["nrn", self.child_col], as_index=False)["prob"].mean()
            self.axon_classified_dict["avg_result"] = test_df3.sort_values(['nrn', 'ID']).reset_index(drop=True)

            # save result
            with open(self.fname_dict[model_name], "wb") as file:
                pickle.dump(self.axon_classified_dict, file=file)

        time.sleep(0.01)


        #post relabel
        if prob_threshold == None :
            print("No probability threshold.")
        elif prob_threshold != None :
            df = self.axon_classified_dict["avg_result"]
            df = post_relabel(df, threshold=self.prob_threshold)

        # save result
        with open(self.fname_dict[model_name], "wb") as file:
            pickle.dump(self.axon_classified_dict, file=file)

        #save result into nrn_pred
        for test_swc_name in self.test_nrn_lst :
            if not os.path.exists(self.input_folder + "nrn_pred/" + test_swc_name + "_predicted" + ".swc"):
                fname_swc = self.input_folder + "nrn_original/" + test_swc_name + ".swc"
                nrn = nm.io.swc.read(fname_swc)
                origin_cols = ['x', 'y', 'z', 'D/2', 'R', 'ID', 'PARENT_ID']
                df0 = pd.DataFrame(nrn.data_block,columns=origin_cols)

                if model_name == 'xgb' :
                    df0['type_xgb'] = df['type_post']
                elif model_name == 'dnn' :
                    df0['type_dnn'] = df['type_post']

            else :
                pre_f = pd.read_csv(self.input_folder + "nrn_pred/" + test_swc_name + "_predicted" + ".swc",
                                    sep=' ',index_col=0)
                df0 = pd.DataFrame(pre_f)

                if model_name == 'xgb' :
                    df0['type_xgb'] = df['type_post']
                elif model_name == 'dnn' :
                    df0['type_dnn'] = df['type_post']

            #save as swc by str
            df0.to_csv(path_or_buf=self.input_folder + 'nrn_pred/' + test_swc_name + "_predicted" + ".swc",
                       na_rep='be cut', sep=' ')

            #save as csv
            df0.to_csv(path_or_buf=self.input_folder + 'nrn_pred/' + test_swc_name + "_predicted" + ".csv",
                       na_rep='be cut')

        return


    def _data_info(self):
        self.axon_classified_dict["info_dict"] = {
            "target": self.classify_target,
            "train_nrn_type": self.train_nrn_type,
            "test_nrn_type": self.test_nrn_type,
            "remove_method": self.remove_method,
            "target_level": self.target_level,
            "mdl_ensemble(only for dnn)": self.mdl_ensemble,
            "features": self.features,
            "pyramid_layer": self.pyr_layer,
            "threshold_layer":self.th_layer,
            "sample": self.sample,
            "sample_with_replacement_times": self.with_replacement,
            "train_nrn_num": len(self.train_set[0]),
            "test_nrn_num": len(self.test_set[0]),
            "post_relabel": self.post_relabel
        }

        return


    def _evaluate(self):
        print("Evaluate models...")

        df0 = None
        bar = progressbar.ProgressBar()
        time.sleep(0.01)

        # 1.
        for m in bar(list(self.models.keys())):

            try:
                self.axon_classified_dict[m]["evaluation_df1"]
            except:
                pass
            else:
                if all([self.post_relabel, not self.overwrite]):
                    continue

            # Full "original" level tree
            if df0 is None:

                ax0 = Prepare_Axon(self.input_folder, self.test_nrn_type, self.remove_method, self.target_level)
                ax0 = ax0.load_data(["forEvaluate"])
                df0 = ax0.prepare_dict["forEvaluate"]
                del ax0
                gc.collect()

            else:
                pass

            cm_lst = []
            acc_lst = []
            df_pr = pd.DataFrame(index=["axon", "dend"], columns=["prc", "rcl"])

            # axon
            df_eval = pd.DataFrame(index=self.prob_thresholds)
            prc_lst = []
            rcl_lst = []
            f1_lst = []

            # dendrite
            df_eval0 = pd.DataFrame(index=self.prob_thresholds)
            prc_lst0 = []
            rcl_lst0 = []
            f1_lst0 = []


            for pred_prob in self.prob_thresholds:
                df = self.axon_classified_dict[m]["avg_result"]
                df = pd.merge(df0, df, how="left", on=['nrn', 'ID'])

                # post_relabel
                if self.post_relabel:
                    df = post_relabel(df, threshold=pred_prob)
                    df = df.loc[(df["NC"]==0)&(df["type_pre"].isin([2,3]))]  # select only "leaf" & "axon and dendrite"
                    y_test = df["type_pre"].values
                    y_pred = df["type_post"].values
                else:
                    df = df.loc[df["type_pre"].isin([2, 3])]  # select all node & "axon and dendrite"
                    y_test = df["type_pre"].values
                    y_pred = df["prob"].values
                    y_pred = np.where(y_pred > pred_prob, 2, 3)

                # evaluate report
                report = classification_report(y_test, y_pred, output_dict=True)
                acc_lst.append(report["accuracy"])
                # axon
                prc_lst.append(report["2"]["precision"])
                rcl_lst.append(report["2"]["recall"])
                f1_lst.append(report["2"]["f1-score"])
                # dendrite
                prc_lst0.append(report["3"]["precision"])
                rcl_lst0.append(report["3"]["recall"])
                f1_lst0.append(report["3"]["f1-score"])


                cm = confusion_matrix(y_test, y_pred, labels=[2,3])
                cm_lst.append(cm)

            df_eval["accuracy"] = acc_lst
            df_eval["precision"] = prc_lst
            df_eval["recall"] = rcl_lst
            df_eval["f1-score"] = f1_lst

            df_eval0["accuracy"] = acc_lst
            df_eval0["precision"] = prc_lst0
            df_eval0["recall"] = rcl_lst0
            df_eval0["f1-score"] = f1_lst0

            h_acc = acc_lst.index(max(acc_lst))
            h_thr = self.prob_thresholds[h_acc]
            df_pr.at["axon", "prc"] = df_eval.iloc[h_acc, 1]
            df_pr.at["axon", "rcl"] = df_eval.iloc[h_acc, 2]
            df_pr.at["dend", "prc"] = df_eval0.iloc[h_acc, 1]
            df_pr.at["dend", "rcl"] = df_eval0.iloc[h_acc, 2]

            self.axon_classified_dict[m]["confusion_matrix"] = cm_lst[0]
            self.axon_classified_dict[m]["confusion_matrix_highAcc"] = cm_lst[h_acc]
            self.axon_classified_dict[m]["evaluation_df1"] = df_eval
            self.axon_classified_dict[m]["evaluation_df0"] = df_eval0
            self.axon_classified_dict[m]["threshold_highAcc"] = h_thr
            self.axon_classified_dict[m]["prcRcl_highAcc"] = df_pr

            # save
            if self.post_relabel:
                with open(self.fname_dict[m], "wb") as file:
                    pickle.dump(self.axon_classified_dict[m], file=file)



        time.sleep(0.01)

        return


    def _show_evaluation(self):
        print_info_dict(self.axon_classified_dict["info_dict"])
        print("===========================================================================================")

        for key, v0 in self.axon_classified_dict.items():
            if key != "info_dict":
                print("Model =", key)

                acc = round(self.axon_classified_dict[key]["evaluation_df1"]["accuracy"].iloc[0], 3)
                print("confusion_matrix (accuracy",str(acc),"): (row: true, col: pred)\n", turn_confusingMatrix_to_DF(self.axon_classified_dict[key]["confusion_matrix"], self.post_relabel), "\n")
                if not self.post_relabel:
                    print(self.axon_classified_dict[key]["prcRcl_highAcc"], "\n")
                if self.post_relabel:
                    max_acc = round(max(self.axon_classified_dict[key]["evaluation_df1"]["accuracy"]), 3)
                    print("confusion_matrix (highest accuracy",str(max_acc),"at thr", round(self.axon_classified_dict[key]["threshold_highAcc"],3), "):\n",
                          turn_confusingMatrix_to_DF(self.axon_classified_dict[key]["confusion_matrix_highAcc"]), "\n")
                    print(self.axon_classified_dict[key]["prcRcl_highAcc"], "\n")

                print("evaluation_df1 (axon):\n", self.axon_classified_dict[key]["evaluation_df1"], "\n")
                print("evaluation_df0 (dendrite):\n", self.axon_classified_dict[key]["evaluation_df0"], "\n")
                print("===========================================================================================")

        return


    def _plot_result(self):
        print("Plotting...")

        df0 = None
        bar = progressbar.ProgressBar()
        time.sleep(0.01)

        # 1.
        for m in bar(list(self.models.keys())):
            # Full "original" level tree
            if df0 is None:

                ax0 = Prepare_Axon(self.input_folder, self.test_nrn_type, self.remove_method, self.target_level)
                ax0 = ax0.load_data(["forTrain"])
                df0 = ax0.prepare_dict["forTrain"]
                del ax0
                gc.collect()
            else:
                pass


            df = self.axon_classified_dict[m]["avg_result"]
            df = pd.merge(df0, df, how="left", on=['nrn', 'ID'])

            # post relabel
            df = post_relabel(df, threshold=self.prob_threshold)

            # plot result level tree
            lst0 = []
            lst1 = []

            for nrn in df.nrn.unique():
                save_path = input_folder + "nrn_plot/result_tree/" + m + "/"

                if self.ground_truth is False:
                    self.type_col = None

                plot_tree_typePost(df.loc[df["nrn"]==nrn], self.child_col, self.parent_col, self.type_col, "type_post", save_path, self.file_type, self.file_size, self.only_terminal, self.branch_col, self.show_node_id)

        return


    def correct_distribution(self, model, prob_threshold):
        '''
        Plot the correct distribution of the prediction of the specific brain area.
        :param model: str. Use 'xgb', 'svm', or 'dnn', etc.
        :param prob_threshold: float. Threshold for post_relabel().
        :return: save plots of correct distribution on the Desktop.
        '''
        nrn_types = ['CIVP']

        for nrn_type in nrn_types:

            # load in classify result
            self._load_data()

            # load in full "original" level tree
            ax0 = Prepare_Axon(self.input_folder, self.test_nrn_type, self.remove_method, self.target_level)
            ax0 = ax0.load_data(["forEvaluate"])
            df0 = ax0.prepare_dict["forEvaluate"]
            del ax0
            gc.collect()

            # choose subsets
            df = self.axon_classified_dict[model]["avg_result"]
            df = pd.merge(df0, df, how="left", on=['nrn', 'ID'])
            if nrn_type == "new_all_2":
                pass
            else:
                df = df.loc[df["nrn"].isin(group_dict[nrn_type])]

            # post_relabel
            df = post_relabel(df, threshold=prob_threshold)
            df = df.loc[(df["NC"] == 0) & (df["type_pre"].isin([2, 3]))]  # select only "leaf" & "axon and dendrite"

            dict0 = {"acc":[], "rcl1":[], "rcl0":[]}
            _lst = df.nrn.unique().tolist()
            for i in _lst:
                df1 = df.loc[df["nrn"]==i].reset_index(drop=True)
                df1["correct"] = np.where(df1["type_pre"]==df1["type_post"], 1, 0)
                y_test = df1["type_pre"].values
                y_pred = df1["type_post"].values

                report = classification_report(y_test, y_pred, output_dict=True)
                dict0["acc"].append(report["accuracy"]*100)
                # axon
                dict0["rcl1"].append(report["2"]["recall"]*100)
                # dendrite
                dict0["rcl0"].append(report["3"]["recall"]*100)


            kde = False
            bins = int(10)  # 20
            hist_kws = {"range": [0, 100]}

            # plot acc distribution
            fig, ax = plt.subplots()
            x = np.array(dict0["acc"])
            sns.distplot(x, ax=ax, kde=kde, label='total', bins=bins, hist_kws=hist_kws)
            plt.legend()
            plt.title(nrn_type+"(nrn_"+str(len(_lst))+")", fontsize=16)
            plt.ylabel('number of nrn', fontsize=14)
            plt.xlabel('accuracy', fontsize=14)
            ax.set_ylim(0, len(x))
            plt.savefig("./data/nrn_plot/feature/" + nrn_type +"_acc_"+str(len(x))+".pdf")
            plt.close()

            # plot prc distribution
            fig, ax = plt.subplots()
            sns.distplot(np.array(dict0["rcl1"]), ax=ax, kde=kde, label='axon', bins=bins, hist_kws=hist_kws, color="g")
            sns.distplot(np.array(dict0["rcl0"]), ax=ax, kde=kde, label="dendrite", bins=bins, hist_kws=hist_kws, color="r")
            plt.legend()
            plt.title(nrn_type+"(nrn_"+str(len(_lst))+")", fontsize=16)
            plt.ylabel('number of nrn', fontsize=14)
            plt.xlabel('recall', fontsize=14)
            ax.set_ylim(0, len(x))
            plt.savefig("./data/nrn_plot/feature/" + nrn_type +"_rcl_"+str(len(x))+".pdf")
            plt.close()

        return


    def analyze_feature(self, model, top_ftrs, prob_threshold=None):
        nrn_types = ["new_all_3"]
        kde = False
        bins = int(100 / 5)
        # prob_threshold = 0.75


        # load in classify result
        self._load_data()
        df_info0 = self.axon_classified_dict[model]["avg_result"]

        # load in full "original" level tree
        ax0 = Prepare_Axon(self.input_folder, self.test_nrn_type, self.remove_method, self.target_level)
        ax0 = ax0.load_data(["forTrain"])
        df0 = ax0.prepare_dict["forTrain"]
        del ax0
        gc.collect()
        df_info1 = df0[['nrn', 'ID', 'type_pre']]
        df0 = pyramid(df0, self.features, self.pyr_layer, self.th_layer, for_train=True, empty_number=-1)
        df0 = pd.merge(df0, df_info0, how="left", on=['nrn', 'ID'])
        df0 = pd.merge(df0, df_info1, how="left", on=['nrn', 'ID'])
        df0 = df0.loc[df0["type_pre"].isin([2, 3])].reset_index(drop=True)

        for nrn_type in nrn_types:
            # choose subsets
            df = df0.loc[df0["nrn"].isin(neuron_dict[nrn_type])].reset_index(drop=True)

            # df0 = df0.loc[df0["nrn"].isin(neuron_dict["lowest_prc_lst"])]    # 0~10%
            # if nrn_type == "Type_LOB_OG":
            #     df = df0.loc[df0["nrn"].isin(group_dict[nrn_type])].reset_index(drop=True)
            # elif nrn_type == "not_Type_LOB_OG":
            #     df = df0.loc[~df0["nrn"].isin(group_dict["Type_LOB_OG"])].reset_index(drop=True)


            for i in range(top_ftrs):
                ftr_rnk = self.axon_classified_dict[model]["ftr_ranking"]
                ftr = ftr_rnk.iloc[i, 0]
                if prob_threshold is None:
                    x1 = df.loc[df["type_pre"] == 2, ftr].to_list()
                    x0 = df.loc[df["type_pre"] == 3, ftr].to_list()
                else:
                    x1 = df.loc[(df["type_pre"]==2)&(df["prob"]>=prob_threshold), ftr].to_list()
                    x0 = df.loc[(df["type_pre"]==3)&(df["prob"]<=(1-prob_threshold)), ftr].to_list()
                r = [max(x1), max(x0), min(x1), min(x0)]
                hist_kws = {"range": [min(r), max(r)]}

                # plot prc distribution
                rnk = "(rnk_"+str(i)+")"
                if prob_threshold is None:
                    name1 = "".join([nrn_type, rnk])
                else:
                    thr = "(thr_"+str(prob_threshold)+")"
                    name1 = "".join([nrn_type, rnk, thr])
                sns.set(color_codes=True)
                sns.distplot(np.array(x1), kde=kde, label='axon', bins=bins, hist_kws=hist_kws)
                sns.distplot(np.array(x0), kde=kde, label="dendrite", bins=bins, hist_kws=hist_kws)
                plt.legend()
                plt.title(name1 + "(nodes_" + str(len(x1+x0)) + ")", fontsize=16)
                plt.ylabel('number of nodes', fontsize=14)
                plt.xlabel(ftr, fontsize=14)
                plt.savefig("./data/nrn_plot/feature/" + name1 + ".pdf")
                plt.close()

            del df

        return




if __name__ == '__main__':
    ax0 = Classify_Axon(input_folder,
                        train_nrn_type,
                        test_nrn_type,
                        remove_method,
                        target_level,
                        models,
                        mdl_ensemble,
                        features,
                        pyramid_layer,
                        threshold_layer,
                        sample,
                        sample_pct,
                        sample_with_replacement,
                        overwrite=overwrite)

    ax0 = ax0.classify_data(rerun_mdl)

    # ax0 = ax0.predict(prob_threshold=0.7)

    # ax0.evaluate(relabel=relabel)

    # ax0.plot_result(prob_threshold=0.7, file_type='jpg', only_terminal=only_terminal, branch_col=branch_col, show_node_id=show_node_id, ground_truth=ground_true)

    # ax0.correct_distribution(model="xgb", prob_threshold=0.75)

    # ax0.analyze_feature(model="xgb", top_ftrs=3)





########################################################################################################################
